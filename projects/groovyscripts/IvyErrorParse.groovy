import java.util.regex.Matcher
import java.util.regex.Pattern

/*
* This script will pick out the test calss names or test class/test names
* from a stack copied from Junit in Eclipse.
*
* First paste test stack into this variable
* Then escape special characters manually,
* (i.e. replace $ with \$) 
* groovyConsole doesn't like them/can't use replace on them.
*/
def test = """\

returns-webservice-at
com.menards.returns.webservice.uat.scenario.rqm.Prd_4957Test
testPrd_fromBO(com.menards.returns.webservice.uat.scenario.rqm.Prd_4957Test)
java.lang.AssertionError: ; ReturnTenderInfo list mismatch on ReturnTender; expected count=1, actual count=1.
Expected: (a collection containing <com.menards.returns.domain.client.Tender@6607a2e0[tenderType=CHARGE_TENDER, checkTenderInfo=<null>, cardTenderInfo=<null>, chargeTenderInfo=com.menards.returns.domain.client.ChargeTenderInfo@7f565474[accountNumber=0]]>)
     but: a collection containing <com.menards.returns.domain.client.Tender@6607a2e0[tenderType=CHARGE_TENDER, checkTenderInfo=<null>, cardTenderInfo=<null>, chargeTenderInfo=com.menards.returns.domain.client.ChargeTenderInfo@7f565474[accountNumber=0]]> was <com.menards.returns.domain.client.Tender@52f37721[tenderType=CHARGE_TENDER, checkTenderInfo=<null>, cardTenderInfo=<null>, chargeTenderInfo=com.menards.returns.domain.client.ChargeTenderInfo@6e124212[accountNumber=1234567890]]>

    at org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)

    at org.junit.Assert.assertThat(Assert.java:865)


"""


//this prints the test class name
def ivysplitTest(def a) {
  a.eachLine() {obj -> def pattern = ~/^com.+/; obj.find(pattern){ match -> println match}} 
}

//this prints the test class name and the test name
def ivysplitAll(def a) {
  a.eachLine() {obj -> def pattern = ~/^com.+|^test.+/; obj.find(pattern){ match -> println match}} 
}


ivysplitTest(test)
//ivysplitAll(test)
