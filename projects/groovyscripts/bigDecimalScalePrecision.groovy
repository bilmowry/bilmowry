String str = "6.10"
BigDecimal fromString = new BigDecimal(str)
Double doub = 6.1
BigDecimal fromDouble = new BigDecimal(doub)
Double fromBigDecimal = fromDouble.doubleValue()

def printInfo(title, inNum) {
   println title + " " + inNum
   if (inNum instanceof Double) {
      String strNum = inNum.toString()
      Integer precision = strNum.length() - 1
      def scale = { clNum ->
         def values = clNum.toString().tokenize(".")
         if(values.size() > 1) {
            return values[1].toString().length()
         } else {
            return 0
         }
      }
      println "      scale: " + scale(inNum) + " precision: " + precision
   } else {
      println "      scale: " + inNum.scale() + " precision: " + inNum.precision()
   }
}

printInfo("string to big-D:", fromString)
printInfo("double to big-D:", fromDouble)
printInfo("big-D to double:", fromBigDecimal)

