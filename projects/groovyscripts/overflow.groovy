//Here's the integer and it's overflow
Integer i = 9876543210
println "overflow error for 9876543210: " + i

//Integer max value
BigInteger maxInt = 2**31 - 1
println "calculated max integer: \t" + maxInt
println "Integer.MAX_VALUE: \t\t" + Integer.MAX_VALUE

//Integer min value
BigInteger minInt = -2**31
println "calculated min integer: \t" + minInt
println "Integer.MIN_VALUE: \t\t" + Integer.MIN_VALUE

//get the Integer range
BigInteger range = maxInt  + 1 - minInt
println "range: \t\t\t" + range

//two trips around the raspberry bush calculation
BigInteger big = 9876543210
println "big: \t\t\t" + big
println "overflow remainder: \t\t" + (big - (2*range))




