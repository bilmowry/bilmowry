/*
from http://www.joelonsoftware.com/articles/GuerrillaInterviewing3.html
Write a function that determines if a string starts with an upper-case letter A-Z
Write a function that determines the area of a circle given the radius
Add up all the values in an array

*/

println "Fortyniners".getAt(0) == "fortyniners".getAt(0).toUpperCase()
println  "fortyniners".toCharacter() >= 64 && "fortyniners".toCharacter() <= 90
println Math.PI * 2
def arrayI = [1,3,5,8]
println arrayI.sum()
