String[] stringArray = ["one","two"];

//First With Lists
def "initialize with lists"() {
    try {
      List<String> list1 =  Arrays.asList("one","two");
      println "array as list to list works"
    } catch (all) {
      println "array as list to list is an error"
    }
    
    try {
      List<String> list2 =  new ArrayList<String>(Arrays.asList("one","two"));
      println "arraylist declaration with arrays as list to list works"
    } catch (all) {
      println "arraylist declaration with arrays as list to list is an error"
    }
    
    try {
      List<Integer> list3 = new ArrayList<String>(Arrays.asList(stringArray));
      println "arraylist declaration with arrays as list to list works"
    } catch (all) {
      println "arraylist declaration with arrays as list to list is an error"
    }
}



//Now With Collections
def "initialize with collections"() {
    try {
      Collections<String> colList1 =  Arrays.asList("one","two");
      println "array as list to collection works"
    } catch (all) {
      println "array as list to collection is an error"
    }
    
    try {
      Collections<String> colList2 =  new ArrayList<String>(Arrays.asList("one","two"));
      println "arraylist declaration with arrays as list to collection works"
    } catch (all) {
      println "arraylist declaration with arrays as list to collection is an error"
    }
    
    try {
      Collections<Integer> colList3 = new ArrayList<String>(stringArray));
      println "arraylist declaration with arrays as list to collection works"
    } catch (all) {
      println "arraylist declaration with arrays as list to collection is an error"
    }
}

"initialize with lists"()
"initialize with collections"()

print "end"