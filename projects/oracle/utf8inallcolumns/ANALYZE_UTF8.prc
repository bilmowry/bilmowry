CREATE OR REPLACE PROCEDURE CUADMIN.ANALYZE_UTF8 IS
BEGIN

DECLARE 

DBMS_MESSAGE_FLAG BOOLEAN := FALSE;   --true write to dbms_output, false write to table
v_message VARCHAR2(400) := '';

--main cursor c1  parameters
v_owner VARCHAR2(24) := 'CUADMIN';
v_start_letter VARCHAR2(1) := 'A';
v_end_letter VARCHAR2(1) := 'Z';

cursor c1 is
select column_name, table_name from all_tab_columns
where data_type in ('CHAR','CLOB','VARCHAR2','RAW') and owner = v_owner
and table_name in (select table_name from all_tables) 
and substr(upper(table_name),1,1) between v_start_letter and v_end_letter
order by table_name, column_name;

--'CHAR','CLOB','VARCHAR2','RAW'
--and table_name = 'UI_CONTENT'

v_count NUMBER := 0;
v_parse_count NUMBER := 0;
v_fail_count NUMBER := 0;

v_sql VARCHAR2(4000);
v_fail VARCHAR2(10);

v_rowid ROWID;
c_clob CLOB;
c_length NUMBER;

TYPE cur_typ IS REF CURSOR;
cursor_table cur_typ;
cursor_algorithm cur_typ;
        
l_buffer VARCHAR2(1000);
l_buffer_size INTEGER := 1000;
l_offset INTEGER := 1;

--for bulk collect
c_bulk_limit NUMBER := 1000;
type string_array is varray(10000) of varchar2(100);
type number_array is varray(10000) of number;
v_rowid_array string_array;
c_length_array number_array;


begin
v_message := 'RUN COMMENCE: ' || to_char(SYSDATE, 'YYYY MON DD HH24:MI:SS');
if DBMS_MESSAGE_FLAG then
    dbms_output.put_line(' ');
    dbms_output.put_line(v_message);
else 
    INSERT INTO UTF8_LOG(ENTRY_DATE,ENTRY) values (SYSDATE,v_message);
    commit;
end if;

for cursor_tab_cols in c1
LOOP
  begin --c_rec cursor block
    --get row id and data for table

   v_sql := 'select rowid, length(' || cursor_tab_cols.COLUMN_NAME || ') from ' || cursor_tab_cols.TABLE_NAME;
   
   v_message := cursor_tab_cols.TABLE_NAME || '.' || cursor_tab_cols.COLUMN_NAME;
   if DBMS_MESSAGE_FLAG then
    dbms_output.put_line(v_message);
   else 
    INSERT INTO UTF8_LOG(ENTRY_DATE,ENTRY) values (SYSDATE,v_message);
    commit;
   end if;

        open cursor_table for v_sql;
        LOOP
            fetch cursor_table bulk collect into v_rowid_array, c_length_array limit c_bulk_limit;
            exit when v_rowid_array.COUNT = 0;
            
            FOR indx IN 1 .. v_rowid_array.COUNT 
            LOOP
                 
                    l_offset:= 1;
                    l_buffer_size := 1000;
                    WHILE l_offset < c_length_array(indx)
                    LOOP
                          if l_offset + l_buffer_size - 1 > c_length_array(indx) then
                              l_buffer_size := c_length_array(indx) - l_offset + 1;
                          end if;
                    
                          v_sql := 'select CASE ' ||
                                   'INSTR (RAWTOHEX (utl_raw.cast_to_raw (utl_i18n.raw_to_char (utl_raw.cast_to_raw (' ||
                                    'SUBSTR(' || cursor_tab_cols.COLUMN_NAME || ','|| l_offset || ',' || l_buffer_size ||')' ||
                                    '), ''utf8''))), ''EFBFBD'') ' ||
                                   'WHEN 0 THEN ''OK'' ' ||
                                   'ELSE ''FAIL'' ' || 
                                   'END  into :into_bind ' ||
                                   'from ' || cursor_tab_cols.TABLE_NAME || ' ' ||
                                   'where rowid = ''' || v_rowid_array(indx) || '''';
                                  
                          --dbms_output.put_line(v_sql);     
                          execute immediate v_sql into v_fail;
                           
                          if v_fail != 'OK' then
                               v_message := ('      ' || v_fail || ' ' || cursor_tab_cols.TABLE_NAME || '.' || cursor_tab_cols.COLUMN_NAME || ' ' || v_rowid_array(indx) || ' ' || c_length_array(indx));
                               if DBMS_MESSAGE_FLAG then
                                 dbms_output.put_line(v_message);
                               else 
                                 INSERT INTO UTF8_LOG(ENTRY_DATE,ENTRY) values (SYSDATE,v_message);
                                 commit;
                               end if;
                               v_fail_count := v_fail_count + 1;
                          end if;

                           v_parse_count := v_parse_count + 1;
                           l_offset := l_offset + l_buffer_size;
                           
                           
                     end loop; -- field parsing loop
                     
                 v_count := v_count + 1;
            END LOOP; --end bulk loop
    --          

       END LOOP;  -- end cursor_table cursor block
       close cursor_table;

  end; --c_rec cursor block
END LOOP;

v_message := 'RUN COMPLETE: ' || to_char(SYSDATE, 'YYYY MON DD HH24:MI:SS');
if DBMS_MESSAGE_FLAG then
    dbms_output.put_line(v_message); 
    dbms_output.put_line('Total parse count: ' || v_parse_count);
    dbms_output.put_line('Total count: ' || v_count);
    dbms_output.put_line('Total fail count: ' || v_fail_count);
else 
    INSERT INTO UTF8_LOG(ENTRY_DATE,ENTRY) values (SYSDATE,v_message);
    INSERT INTO UTF8_LOG(ENTRY_DATE,ENTRY) values (SYSDATE,'Total parse count: ' || v_parse_count);
    INSERT INTO UTF8_LOG(ENTRY_DATE,ENTRY) values (SYSDATE,'Total count: ' || v_count);
    INSERT INTO UTF8_LOG(ENTRY_DATE,ENTRY) values (SYSDATE,'Total fail count: ' || v_fail_count);
    commit;
end if;

end;

END ANALYZE_UTF8;
/