

--list out the types of languages allowed

--list out character sets
SELECT value$ FROM sys.props$ WHERE name = 'NLS_CHARACTERSET' ;

select value from nls_database_parameters where parameter = 'NLS_CHARACTERSET';

select value from nls_database_parameters where parameter = 'NLS_NCHAR_CHARACTERSET';


SELECT * FROM NLS_DATABASE_PARAMETERS;

SELECT * FROM V$NLS_VALID_VALUES WHERE parameter = 'CHARACTERSET' order by 3 desc


--list out the counts of data columns
select count(*), data_type from all_tab_columns  where owner = 'CUADMIN' group by data_type order by data_type;