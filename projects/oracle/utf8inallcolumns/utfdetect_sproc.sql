/***********************************************
** ANALYZE_UTF8
** Purpose: this will look at all the character type
** fields in a schema to see if they are UTF-8 recognizable.
** If not, an error is written to either to output or a table.
**
** USAGE
** --truncate and drop the log table if it exists, write to output, exit program
**   exec analyze_utf8('CUADMIN','','',true,'D');
**
** --create log table if it doesn't exist, write to output, exit program
**   exec analyze_utf8('CUADMIN','','',true,'C');
**
** --truncate table, process tables starting with A to J, write to table, exit program
**   exec analyze_utf8('CUADMIN','A','J',false,'C');

** --do nothing to log table, process tables starting with K to Z, write to table, exit program
**   exec analyze_utf8('CUADMIN','K','Z',false,'P');
***********************************************/
CREATE OR REPLACE PROCEDURE CUADMIN.ANALYZE_UTF8(
p_schema_owner varchar2,
p_start_letter varchar2 := 'A',
p_end_letter varchar2 := 'Z',
p_dbms_message_flag boolean := true,
p_table_event varchar2 := 'P'
)
IS
BEGIN

DECLARE 

v_owner VARCHAR2(24) := p_schema_owner;
v_start_letter VARCHAR2(1) := p_start_letter;  --starting letter of table name to process
v_end_letter VARCHAR2(1) := p_end_letter; --ending letter of table name to process
DBMS_MESSAGE_FLAG BOOLEAN := p_dbms_message_flag; --true =  write to dbms_output, false = write to table
v_table_event varchar2(1) := p_table_event; --table directives: P=proceed with processing, C=create, T=truncate, D=drop

cursor cursor_table_list is
select column_name, table_name from all_tab_columns
where data_type in ('CHAR','CLOB','VARCHAR2','RAW') and owner = v_owner
and table_name in (select table_name from all_tables) 
and substr(upper(table_name),1,1) between upper(v_start_letter) and upper(v_end_letter)
order by table_name, column_name;

TYPE cur_typ IS REF CURSOR;
cursor_table cur_typ;

v_message VARCHAR2(400) := '';
v_count NUMBER := 0;
v_parse_count NUMBER := 0;
v_fail_count NUMBER := 0;

v_sql VARCHAR2(4000);
v_fail VARCHAR2(10);

v_rowid ROWID;
c_length NUMBER;

l_buffer_size INTEGER := 1000;
l_offset INTEGER := 1;

function table_event(p_fnc_owner in varchar2, p_fnc_table_event in varchar2) RETURN VARCHAR2 is
v_rtrn varchar2(8);
begin
      CASE p_fnc_table_event
        WHEN 'C' THEN --create table --> assuming user is same as schema
            EXECUTE IMMEDIATE 'GRANT CREATE TABLE TO ' || p_fnc_owner;
            EXECUTE IMMEDIATE 'CREATE TABLE ' || p_fnc_owner || '.UTF8_LOG ' ||
                         '(ENTRY_DATE TIMESTAMP(6),ENTRY VARCHAR2(400))';           
            RETURN 'EXIT';
        WHEN 'T' THEN --truncate table; this proceeds with analysys
            EXECUTE IMMEDIATE 'TRUNCATE TABLE ' || p_fnc_owner || '.UTF8_LOG';
            RETURN '';     
        WHEN 'D' THEN --drop table
            v_rtrn := table_event(p_fnc_owner,'T');
            EXECUTE IMMEDIATE 'DROP TABLE ' || p_fnc_owner || '.UTF8_LOG';
            RETURN 'EXIT';   
        ELSE RETURN '';
      END CASE;
end table_event;

procedure log_event(p_fnc_owner in varchar2, p_log_message in varchar2) is
v_msg_sql VARCHAR2(4000) := '';
begin
    v_msg_sql := 'INSERT INTO ' || p_fnc_owner || '.UTF8_LOG(ENTRY_DATE,ENTRY) '||
                 'values (SYSDATE,''' || v_message || ''')';
    EXECUTE IMMEDIATE v_msg_sql;
end log_event;

begin--first see if there is anything to do with the table, bail on create or drop

    if table_event(v_owner,v_table_event) = 'EXIT' then 
        return;
    end if;

    v_message := 'RUN COMMENCE: ' || to_char(SYSDATE, 'YYYY MON DD HH24:MI:SS');
    if DBMS_MESSAGE_FLAG then
        dbms_output.put_line(' ');
        dbms_output.put_line(v_message);
    else 
        log_event(v_owner,v_message);
    end if;

    for cursor_tab_cols in cursor_table_list
    LOOP
      begin 
       v_sql := 'select rowid, length(' || cursor_tab_cols.COLUMN_NAME || ') from ' || cursor_tab_cols.TABLE_NAME;
       
       v_message := cursor_tab_cols.TABLE_NAME || '.' || cursor_tab_cols.COLUMN_NAME;
       if DBMS_MESSAGE_FLAG then
        dbms_output.put_line(v_message);
       else 
        log_event(v_owner,v_message);
       end if;

        open cursor_table for v_sql;
        LOOP
            fetch cursor_table into v_rowid, c_length;
            exit when cursor_table%NOTFOUND;
                 
            l_offset:= 1;
            l_buffer_size := 1000;
            WHILE l_offset < c_length
            LOOP
                  if l_offset + l_buffer_size - 1 > c_length then
                      l_buffer_size := c_length - l_offset + 1;
                  end if;
            
                  v_sql := 'select CASE ' ||
                           'INSTR (RAWTOHEX (utl_raw.cast_to_raw (utl_i18n.raw_to_char (utl_raw.cast_to_raw (' ||
                            'SUBSTR(' || cursor_tab_cols.COLUMN_NAME || ','|| l_offset || ',' || l_buffer_size ||')' ||
                            '), ''utf8''))), ''EFBFBD'') ' ||
                           'WHEN 0 THEN ''OK'' ' ||
                           'ELSE ''FAIL'' ' || 
                           'END  into :into_bind ' ||
                           'from ' || v_owner || '.' || cursor_tab_cols.TABLE_NAME || ' ' ||
                           'where rowid = ''' || v_rowid || '''';
                          
                  --dbms_output.put_line(v_sql);     
                  execute immediate v_sql into v_fail;
                   
                  if v_fail != 'OK' then
                       v_message := ('      ' || v_fail || ' ' || cursor_tab_cols.TABLE_NAME || '.' || cursor_tab_cols.COLUMN_NAME || ' ' || v_rowid || ' ' || c_length);
                       if DBMS_MESSAGE_FLAG then
                         dbms_output.put_line(v_message);
                       else 
                         log_event(v_owner,v_message);
                       end if;
                       v_fail_count := v_fail_count + 1;
                  end if;

                   v_parse_count := v_parse_count + 1;
                   l_offset := l_offset + l_buffer_size;
              end loop; -- field parsing loop
    --          
              v_count := v_count + 1;
           END LOOP; 
           close cursor_table;

      end;
    END LOOP;

    v_message := 'RUN COMPLETE: ' || to_char(SYSDATE, 'YYYY MON DD HH24:MI:SS');
    if DBMS_MESSAGE_FLAG then
        dbms_output.put_line(v_message); 
        dbms_output.put_line('Total parse count: ' || v_parse_count);
        dbms_output.put_line('Total count: ' || v_count);
        dbms_output.put_line('Total fail count: ' || v_fail_count);
    else 
        log_event(v_owner,v_message);
        log_event(v_owner,'Total parse count: ' || v_parse_count);
        log_event(v_owner,'Total count: ' || v_count);
        log_event(v_owner,'Total fail count: ' || v_fail_count);
    end if;

end;


END ANALYZE_UTF8;
/