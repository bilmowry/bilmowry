//Expands fileUpload button for PrimeFaces to it's parent container
//Make sure to check ho PrimeFaces generates id's
function formatFileUpload(data) {
    var control = $('#' + data.formId + '\\:' + data.buttonId);
    control.height(control.parent().height());
    control.width(control.parent().width());
}

//Positions an instructions text relative to the upload button
function formatFileInstructions(data) {
    var control = $('#' + data.formId + '\\:' + data.instructionsId);
    control.position({
        my:'left top',
        at:'right top',
        of:$('#' + data.formId + '\\:' + data.buttonId),
        offset:data.offsetLeft + ' ' + data.offsetTop,
        collision:'none'
    });
}

//Initializes events and behaviors
function initializeFileUpload(data) {
    $(window).load(function () {
        formatFileUpload(data);
        formatFileInstructions(data);
    });
    $(document).ready(function(){
        $(document).on('DOMNodeRemoved', function () {
            formatFileInstructions(data);
        });
        $(document).on('change', function () {
            formatFileInstructions(data);
        });
    });
}
