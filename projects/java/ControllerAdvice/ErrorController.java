package com.menards.pos.ws.common.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.menards.pos.ws.common.web.response.ControllerResponse;
import com.menards.pos.ws.common.web.response.ControllerResponseEnum;
//
@Controller
public class ErrorController {
  
//    @RequestMapping(value="/**")
//    public void unmappedRequest(HttpServletResponse response) throws IOException {
//	response.setStatus(HttpServletResponse.SC_NOT_FOUND);
//	response.getWriter().write("404 error message");
//    }
    
    @RequestMapping(value="/**")
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody ControllerResponse unmappedRequest(HttpServletResponse response) throws IOException {
	return new ControllerResponse.Builder().status(ControllerResponseEnum.error).message(HttpStatus.NOT_FOUND.getReasonPhrase()).build();
    }
    
}
