package com.menards.pos.ws.common.web.response;

public enum ControllerResponseEnum {
    success, fail, error
}
