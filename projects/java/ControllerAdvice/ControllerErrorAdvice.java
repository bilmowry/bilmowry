package com.menards.pos.ws.common.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.menards.pos.ws.common.web.response.ControllerResponse;
import com.menards.pos.ws.common.web.response.ControllerResponseEnum;

@ControllerAdvice
public class ControllerErrorAdvice {

    private final Logger log = LoggerFactory.getLogger(ControllerErrorHandler.class);

    @ExceptionHandler({ Throwable.class })
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ControllerResponse handleThrowable(Throwable pe) {
	log.debug("ControllerErrorHandler: Exception");
	return new ControllerResponse.Builder().status(ControllerResponseEnum.error).message(pe.getMessage()).build();
    }

}
