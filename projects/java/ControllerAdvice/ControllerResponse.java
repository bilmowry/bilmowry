package com.menards.pos.ws.common.web.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class ControllerResponse {

    @JsonProperty
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ControllerResponseEnum status;
    @JsonProperty
    private String data;
    @JsonProperty
    private String meta;
    @JsonProperty
    private String message;
    
    private ControllerResponse(Builder builder) {
	this.status = builder.status;
	this.data = builder.data;
	this.meta = builder.meta;
	this.message = builder.message;
    }

    public ControllerResponseEnum getStatus() {
	return status;
    }

    public void setStatus(ControllerResponseEnum status) {
	this.status = status;
    }

    public String getData() {
	return data;
    }

    public void setData(String data) {
	this.data = data;
    }

    public String getMeta() {
	return meta;
    }

    public void setMeta(String meta) {
	this.meta = meta;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    public static class Builder {
	private ControllerResponseEnum status;
	private String data;
	private String meta;
	private String message;
	
	public Builder () {};
	
	public Builder status(ControllerResponseEnum status) {
	    this.status = status;
	    return this;
	}
	
	public Builder data(String data) {
	    this.data = data;
	    return this;
	}
	public Builder meta(String meta) {
	    this.meta = meta;
	    return this;
	}
	
	public Builder message(String message) {
	    this.message = message;
	    return this;
	}
	
	public ControllerResponse build() {
	    return new ControllerResponse(this);
	}

    }

}
