package com.menards.pos.ws.common.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.menards.pos.ws.common.domain.entity.json.overstock.PalletHeader;
import com.menards.pos.ws.common.domain.entity.json.overstock.request.OverstockPalletRemoveRequest;
import com.menards.pos.ws.common.domain.entity.json.overstock.request.OverstockPalletRestoreRequest;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletAddResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletDeletedResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletListResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletRemoveResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletRestoreResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletUpdateResponse;
import com.menards.pos.ws.common.service.overstock.impl.OverstockPalletServiceImpl;

@Controller
@RequestMapping(value = "/pallet", produces = MediaType.APPLICATION_JSON_VALUE)
public class OverstockPalletController {
    private final Logger log = LoggerFactory.getLogger(OverstockPalletController.class);

    @Autowired
    private OverstockPalletServiceImpl overstockPalletService;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    OverstockPalletListResponse list() {
	return overstockPalletService.list();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{palletId}")
    public @ResponseBody
    PalletHeader find(@PathVariable Integer palletId) {
	return overstockPalletService.find(palletId);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{palletId}", headers = "content-type=application/xml", produces = MediaType.APPLICATION_XML_VALUE)
    public @ResponseBody
    String findXML(@PathVariable Integer palletId) throws JsonProcessingException {
	XmlMapper xmlMapper = new XmlMapper();
	String xml = xmlMapper.writeValueAsString(overstockPalletService.find(palletId));
	return xml;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/remove")
    public @ResponseBody
    OverstockPalletDeletedResponse removed() {
	return overstockPalletService.removed();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/remove/{palletId}")
    public @ResponseBody
    PalletHeader remove(@PathVariable Integer palletId) {
	return overstockPalletService.removedById(palletId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/remove")
    public @ResponseBody
    OverstockPalletRemoveResponse delete(@RequestBody OverstockPalletRemoveRequest request) {
	return overstockPalletService.remove(request);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/restore")
    public @ResponseBody
    OverstockPalletRestoreResponse restore(@RequestBody OverstockPalletRestoreRequest request) {
	return overstockPalletService.restore(request);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    OverstockPalletAddResponse add(@RequestBody PalletHeader pallet) {
	log.debug("Adding new pallet");

	return overstockPalletService.add(pallet);
    }

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    OverstockPalletUpdateResponse update(@RequestBody PalletHeader pallet) {
	log.debug("Updating pallet {}", pallet.getPalletId());

	return overstockPalletService.update(pallet);
    }
}