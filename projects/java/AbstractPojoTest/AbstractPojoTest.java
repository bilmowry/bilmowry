package com.menards.pos.ws.common.test;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;

import com.menards.pos.ws.common.domain.value.Decimal;

public abstract class AbstractPojoTest {

    private Map<Class<?>, Object> testValues = new HashMap<Class<?>, Object>();

    protected void addTestValue(Class<?> propertyType, Object testValue) {
	testValues.put(propertyType, testValue);
    }

    @Before
    public void setUpTestValues() throws Exception {

	addTestValue(String.class, "foo");
	addTestValue(int.class, 123);
	addTestValue(Integer.class, 123);
	addTestValue(double.class, 123.0);
	addTestValue(Double.class, 123.0);
	addTestValue(boolean.class, true);
	addTestValue(Boolean.class, true);
	addTestValue(java.util.Date.class, new java.util.Date(1380209538000L));
	addTestValue(Decimal.class, new Decimal("1234.99"));
    }

    @SuppressWarnings("unchecked")
    public <T> T generatePojo(Class<?> pojoClass, String... ignoreProperties) {
	try {
	    Object pojo = pojoClass.newInstance();
	    BeanInfo pojoInfo = Introspector.getBeanInfo(pojoClass);
	    for (PropertyDescriptor propertyDescriptor : pojoInfo.getPropertyDescriptors()) {
		if (!Arrays.asList(ignoreProperties).contains(propertyDescriptor.getName())) {
		    Class<?> propertyType = propertyDescriptor.getPropertyType();
		    Object testValue = testValues.get(propertyType);
		    if (testValue != null) {
			Method writeMethod = propertyDescriptor.getWriteMethod();
			Method readMethod = propertyDescriptor.getReadMethod();
			if (readMethod != null && writeMethod != null) {
			    writeMethod.invoke(pojo, testValue);
			    Assert.assertEquals(readMethod.invoke(pojo), testValue);
			}
		    }

		}
	    }
	    return (T) pojo;
	} catch (Exception e) {
	    // ignore
	}
	return null;
    }

    /*
     * ignoreProperties not required Valid method calls: testPojo(pojoClass)
     * testPojo(pojoClass, "propName1", "propName2")
     */
    protected void testPojo(Class<?> pojoClass, String... ignoreProperties) {
	try {
	    Object pojo = pojoClass.newInstance();
	    BeanInfo pojoInfo = Introspector.getBeanInfo(pojoClass);
	    for (PropertyDescriptor propertyDescriptor : pojoInfo.getPropertyDescriptors()) {
		if (!Arrays.asList(ignoreProperties).contains(propertyDescriptor.getName())) {
		    testProperty(pojo, propertyDescriptor);
		}
	    }
	} catch (Exception e) {
	    // ignore
	}
    }

    private void testProperty(Object pojo, PropertyDescriptor propertyDescriptor) {
	try {
	    Class<?> propertyType = propertyDescriptor.getPropertyType();
	    Object testValue = testValues.get(propertyType);
	    if (testValue == null) {
		return;
	    }
	    Method writeMethod = propertyDescriptor.getWriteMethod();
	    Method readMethod = propertyDescriptor.getReadMethod();
	    if (readMethod != null && writeMethod != null) {
		writeMethod.invoke(pojo, testValue);
		Assert.assertEquals(readMethod.invoke(pojo), testValue);
	    }
	} catch (Exception e) {
	    // ignore
	}
    }
}