package com.menards.pos.ws.common.domain.dto.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.menards.pos.ws.common.domain.value.Decimal;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonTypeName("itemInfo")
public class ItemInfoDto {

	@JsonProperty
	private Decimal sku;
	@JsonProperty
	private Decimal prepaidQuantity;
	@JsonProperty
	private Decimal overstockQuantity;
	@JsonProperty
	private String notes;

	public Decimal getSku() {
		return sku;
	}

	public void setSku(Decimal sku) {
		this.sku = sku;
	}

	public Decimal getPrepaidQuantity() {
		return prepaidQuantity;
	}

	public void setPrepaidQuantity(Decimal prepaidQuantity) {
		this.prepaidQuantity = prepaidQuantity;
	}

	public Decimal getOverstockQuantity() {
		return overstockQuantity;
	}

	public void setOverstockQuantity(Decimal overstockQuantity) {
		this.overstockQuantity = overstockQuantity;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
