package com.menards.pos.ws.common.domain.model;

import com.menards.pos.ws.common.domain.entity.handy.ItemHistEntity;
import com.menards.pos.ws.common.domain.entity.handy.ItemPluEntity;
import com.menards.pos.ws.common.domain.entity.handy.ItemReplenEntity;
import com.menards.pos.ws.common.domain.entity.menard.ItemInfoEntity;
import com.menards.pos.ws.common.domain.value.Decimal;

public class SkuEntity {
	
	private Decimal sku;
	
	private ItemPluEntity itemPluEntity;
	
	private ItemReplenEntity itemReplenEntity;
	
	private ItemInfoEntity itemInfoEntity;
	
	private ItemHistEntity itemHistEntity;
	
	public SkuEntity(Decimal sku) {
		this.sku = sku;
	}
	
	public SkuEntity() {
	    // TODO Auto-generated constructor stub
	}

	public Decimal getSku() {
		return sku;
	}

	public void setSku(Decimal sku) {
		this.sku = sku;
	}

	public ItemPluEntity getItemPluEntity() {
		return itemPluEntity;
	}

	public void setItemPluEntity(ItemPluEntity itemPluEntity) {
		this.itemPluEntity = itemPluEntity;
	}

	public ItemReplenEntity getItemReplenEntity() {
		return itemReplenEntity;
	}

	public void setItemReplenEntity(ItemReplenEntity itemReplenEntity) {
		this.itemReplenEntity = itemReplenEntity;
	}

	public ItemInfoEntity getItemInfoEntity() {
		return itemInfoEntity;
	}

	public void setItemInfoEntity(ItemInfoEntity itemInfoEntity) {
		this.itemInfoEntity = itemInfoEntity;
	}

	public ItemHistEntity getItemHistEntity() {
	    return itemHistEntity;
	}

	public void setItemHistEntity(ItemHistEntity itemHistEntity) {
	    this.itemHistEntity = itemHistEntity;
	}
	
}
