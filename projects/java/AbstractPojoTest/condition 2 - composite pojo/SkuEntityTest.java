package com.menards.pos.ws.common.domain.model;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.menards.pos.ws.common.domain.entity.handy.ItemHistEntity;
import com.menards.pos.ws.common.domain.entity.handy.ItemPluEntity;
import com.menards.pos.ws.common.domain.entity.handy.ItemReplenEntity;
import com.menards.pos.ws.common.domain.entity.menard.ItemInfoEntity;
import com.menards.pos.ws.common.domain.value.Decimal;
import com.menards.pos.ws.common.test.AbstractPojoTest;

public class SkuEntityTest extends AbstractPojoTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() {
	
	ItemPluEntity itemPluEntity = generatePojo(ItemPluEntity.class);
	ItemReplenEntity itemReplenEntity = generatePojo(ItemReplenEntity.class);
	ItemInfoEntity itemInfoEntity = generatePojo(ItemInfoEntity.class);
	ItemHistEntity itemHistEntity = generatePojo(ItemHistEntity.class);
	
	SkuEntity entity = new SkuEntity(new Decimal(555));
	assertEquals(new Decimal(555), entity.getSku());
	
	entity = null;
	entity = new SkuEntity();
	entity.setSku(new Decimal(555));
	assertEquals(new Decimal(555), entity.getSku());
	
	entity.setItemPluEntity(itemPluEntity);
	entity.setItemReplenEntity(itemReplenEntity);
	entity.setItemInfoEntity(itemInfoEntity);
	entity.setItemHistEntity(itemHistEntity);
	
	assertEquals(true, entity.getItemPluEntity().getFoodItem());
	assertEquals("foo", entity.getItemReplenEntity().getDcShipCode());
	assertEquals(Integer.valueOf(123), entity.getItemInfoEntity().getEstArrivalDays());
	assertEquals(new Decimal(1234.99), entity.getItemHistEntity().getSku());
	
    }

}
