package com.menards.pos.ws.common.domain.dto.mapper.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.menards.pos.ws.common.domain.dto.entity.SkuLookupDto;
import com.menards.pos.ws.common.domain.dto.mapper.SkuLookupMapper;
import com.menards.pos.ws.common.domain.entity.handy.ItemPluEntity;
import com.menards.pos.ws.common.domain.entity.menard.ItemInfoEntity;
import com.menards.pos.ws.common.domain.model.SkuLookupEntity;
import com.menards.pos.ws.common.domain.value.Decimal;
import com.menards.pos.ws.common.test.AbstractPojoTest;

public class SkuLookupMapperImplTest extends AbstractPojoTest {

    private SkuLookupMapper skuLookupMapper = new SkuLookupMapperImpl();

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testNullEntity() {

	SkuLookupDto skuLookupDto = skuLookupMapper.toDTO(null, SkuLookupDto.class);

	assertTrue(null == skuLookupDto.getSku());
	assertTrue(null == skuLookupDto.getCombDesc());

    }

    @Test
    public void testToDTO() {

	SkuLookupDto skuLookupDto = skuLookupMapper.toDTO(makeEntity(), SkuLookupDto.class);

	assertEquals(skuLookupDto.getRetailPrice(), new Decimal(1234.99));

    }

    private SkuLookupEntity makeEntity() {
	SkuLookupEntity skuLookupEntity = generatePojo(SkuLookupEntity.class);
	skuLookupEntity.setItemInfoEntity((ItemInfoEntity) generatePojo(ItemInfoEntity.class));
	skuLookupEntity.setItemPluEntity((ItemPluEntity) generatePojo(ItemPluEntity.class));

	return skuLookupEntity;
    }

}
