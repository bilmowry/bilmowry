package com.menards.pos.ws.common.test;

import java.util.ArrayList;
import java.util.List;

import com.menards.pos.ws.common.domain.dto.entity.PalletDetailDto;
import com.menards.pos.ws.common.domain.dto.mapper.PalletDetailMapper;
import com.menards.pos.ws.common.domain.dto.mapper.impl.PalletDetailMapperImpl;
import com.menards.pos.ws.common.domain.entity.menard.PalletDetailEntity;
import com.menards.pos.ws.common.domain.entity.menard.PalletHeaderEntity;

public class AbstractPalletDetailFixture extends AbstractPojoTest{
    
    protected PalletDetailMapper palletDetailMapper = new PalletDetailMapperImpl();

    protected Integer palletId = new Integer(11);

    protected Integer sku = new Integer(6000007);

    protected PalletDetailDto palletDetailDtoA;

    protected PalletDetailDto palletDetailDtoB;

    protected PalletDetailEntity palletDetailEntity;

    protected PalletHeaderEntity palletHeaderEntity = new PalletHeaderEntity();

    protected List<PalletDetailEntity> palletDetailEntities = new ArrayList<PalletDetailEntity>();

    protected List<PalletDetailDto> palletDetailDTOs = new ArrayList<PalletDetailDto>();
    
    @SuppressWarnings("serial")
    public void initPalletDetailSetup() {
	palletDetailDtoA = this.generatePojo(PalletDetailDto.class);
	palletDetailDtoA.setPalletId(palletId);
	palletDetailDtoA.setSku(sku);
	palletDetailEntity = palletDetailMapper.fromDTO(palletDetailDtoA, PalletDetailEntity.class);

	palletHeaderEntity.setPalletId(palletId);
	palletHeaderEntity.setPalletDetailEntities(new ArrayList<PalletDetailEntity>() {
	    {
		add(palletDetailEntity);
	    }
	});
	palletDetailEntity.setPalletHeaderEntity(palletHeaderEntity);

	palletDetailDtoB = this.generatePojo(PalletDetailDto.class);
	palletDetailDtoB.setPalletId(palletId);
	palletDetailDtoB.setSku(sku + 10);

	palletDetailEntities.add(palletDetailEntity);
	palletDetailEntities.add(palletDetailMapper.fromDTO(palletDetailDtoB, PalletDetailEntity.class));

	palletDetailDTOs = palletDetailMapper.fromDTOs(palletDetailEntities, PalletDetailDto.class);
    }

}
