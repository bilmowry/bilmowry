package com.menards.pos.ws.common.domain.entity.menard;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.menards.pos.ws.common.domain.TableNames;
import com.menards.pos.ws.common.domain.entity.EntityBase;
import com.menards.pos.ws.common.domain.factory.DecimalUserType;
import com.menards.pos.ws.common.domain.value.Decimal;

@Entity
@Table(name = TableNames.PALLET_DETAIL)
public class PalletDetailEntity extends EntityBase
{
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PalletDetailPk entityId;

	@Column(name = "OverstockQty")
	@Type(type = DecimalUserType.TYPE_VALUE)
	private Decimal overstockQty;

	@Column(name = "PulledDate")
	@Type(type = "com.menards.pos.ws.common.util.ProgressTimestampConverter")
	private Date pulledDate;
	
	@Column(name = "PulledTime")
	private String pulledTime;

	@Column(name = "NeedToPullFlag")
	private Boolean needtoPullFlag;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PalletID",insertable=false, updatable=false)
	private PalletHeaderEntity palletHeaderEntity;

	public PalletDetailEntity()
	{
		// no code
	}

	public final PalletDetailPk getEntityId()
	{
		return entityId;
	}
	
	public final void setEntityId(PalletDetailPk entityId)
	{
		this.entityId = entityId;
	}

	public final Decimal getOverstockQty()
	{
		return overstockQty;
	}

	public final void setOverstockQty(Decimal overstockQty)
	{
		this.overstockQty = overstockQty;
	}

	public final Date getPulledDate()
	{
		return pulledDate;
	}

	public final void setPulledDate(Date pulledDate)
	{
		this.pulledDate = pulledDate;
	}

	public final String getPulledTime()
	{
		return pulledTime;
	}

	public final void setPulledTime(String pulledTime)
	{
		this.pulledTime = pulledTime;
	}

	public final Boolean getNeedtoPullFlag()
	{
		return needtoPullFlag;
	}

	public final void setNeedtoPullFlag(Boolean needtoPullFlag)
	{
		this.needtoPullFlag = needtoPullFlag;
	}

	public final PalletHeaderEntity getPalletHeaderEntity()
	{
		return palletHeaderEntity;
	}

	public final void setPalletHeaderEntity(PalletHeaderEntity palletHeaderEntity)
	{
		this.palletHeaderEntity = palletHeaderEntity;
	}	
	
}