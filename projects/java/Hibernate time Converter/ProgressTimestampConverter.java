package com.menards.pos.ws.common.util;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Progress timestamp_timesone comes in a s String from the JDBC driver, 
 * this converts it to a date in a Hibernate entity.
 * Usage: Add the following annotation to your Date property:
 * 
 * @Type(type = "com.menards.pos.ws.common.util.ProgressTimestampConverter")
 * 
 */
public class ProgressTimestampConverter implements UserType {

    private final Logger log = LoggerFactory.getLogger(ProgressTimestampConverter.class);
    
    private static String ZONE_FORMAT = "yyyy-MM-dd HH:mm:ss:SSS Z";

    @Override
    public int[] sqlTypes() {
	return new int[] { Types.VARCHAR };
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Class returnedClass() {
	return ProgressTimestampConverter.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
	if(x == null && y == null) {
	    return true;
	} else if((x == null && y != null) || (x != null && y == null)) {
	    return true;
	}
	return x.equals(y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
	if(x == null) {
	    return 0;
	}
	return x.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner)
	    throws HibernateException, SQLException {
	String value = rs.getString(names[0]);
	if (value == null) {
	    return null;
	}
	try {
	    Date date = new SimpleDateFormat(ZONE_FORMAT).parse(fromProgressTimestampTimezone(value));
	    return date;
	} catch (ParseException e) {
	    e.printStackTrace();
	}
	return null;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session)
	    throws HibernateException, SQLException {
	if (value == null) {
	    st.setString(index, null);
	    return;
	}
	if (!(value instanceof java.util.Date)) {
	    throw new UnsupportedOperationException("can't convert " + value.getClass());
	}
	String dateStr = toProgressTimestampTimezone(new SimpleDateFormat(ZONE_FORMAT).format(value));
	st.setString(index, dateStr);
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
	if (value == null)
	    return null;
	return value;
    }

    @Override
    public boolean isMutable() {
	return true;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
	return (Serializable) value;
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
	return cached;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
	return original;
    }

    private String fromProgressTimestampTimezone(String dateString) {
	// comes in "2013-11-06 00:00:00:000 - 06:00"
	// goes out "2013-11-06 00:00:00:000 -0600"
	String start = dateString.substring(0, dateString.length() - 7);
	String timezone = dateString.substring(dateString.length() - 7);
	timezone = timezone.replace(":", "");
	timezone = timezone.replace(" ", "");
	return start + timezone;
    }

    private String toProgressTimestampTimezone(String dateString) {
	// comes in "2013-11-06 00:00:00:000 -0600"
	// goes out "2013-11-06 00:00:00:000 - 06:00"
	String start = dateString.substring(0, dateString.length() - 5);
	String timezone = dateString.substring(dateString.length() - 5);
	timezone = timezone.substring(0, 1) + " " + timezone.substring(2);
	timezone = timezone.substring(0, timezone.length() - 2) + ":" + timezone.substring(timezone.length() - 2);
	return start + timezone;
    }

}
