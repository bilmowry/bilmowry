package com.menards.pos.ws.common.test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;

public abstract class AbstractEnumTest {

    public <E extends Enum<E>> void testEnumConstants(Class<E> enumClass, String... testValues) {
	String[] enumList = Arrays.toString(enumClass.getEnumConstants()).replaceAll("\\[|]", "").split(", ");
	compareArrays(enumList, testValues);
    }

    public <E extends Enum<E>> void testEnumValues(Class<E> enumClass, String... testValues) {
	try {
	    List<String> enumList = new ArrayList<String>();
	    for (E enoom : Arrays.asList(enumClass.getEnumConstants())) {
		Method method = enumClass.getMethod("getValue");
		enumList.add(method.invoke(enoom).toString());
	    }
	    compareArrays(enumList.toArray(new String[enumList.size()]), testValues); 
	} catch (Exception e) {
	    String msg = "AbstractEnumTest.testEnumValues() error: " + e.getLocalizedMessage();
	    System.out.println(msg);
	    Assert.fail(msg);
	}
    }

    private void compareArrays(String[] enumValues, String... testValues) {
	List<String> enumList = new LinkedList<String>(Arrays.asList(enumValues));
	List<String> compareList = new LinkedList<String>(Arrays.asList(testValues));
	Assert.assertEquals(enumList.size(), compareList.size());
	compareList.removeAll(enumList);
	Assert.assertEquals(0, compareList.size());
    }

}
