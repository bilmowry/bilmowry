package com.menards.pos.ws.common.domain;

public enum POStatusEnum {

    A("Auto Can"), C("Canceled"), F("Completed"), O("Open"), P("Partial"), H("Held");

    private String value;
    
    private POStatusEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
