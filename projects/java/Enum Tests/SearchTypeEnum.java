package com.menards.pos.ws.common.domain;

public enum SearchTypeEnum {

	KEYWORDS, PHRASES, SKUEXACTS, SKUPARTIALS

}
