package com.menards.pos.ws.common.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.menards.pos.ws.common.test.AbstractEnumTest;

public class SearchTypeEnumTest extends AbstractEnumTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test()  {
	testEnumConstants(SearchTypeEnum.class, "KEYWORDS", "PHRASES", "SKUEXACTS", "SKUPARTIALS");
    }
    


}
