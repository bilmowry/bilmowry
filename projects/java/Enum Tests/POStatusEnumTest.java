package com.menards.pos.ws.common.domain;

import java.lang.reflect.InvocationTargetException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.menards.pos.ws.common.test.AbstractEnumTest;

public class POStatusEnumTest extends AbstractEnumTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() {
	testEnumConstants(POStatusEnum.class, "A", "C", "F", "O", "P", "H");
	testEnumValues(POStatusEnum.class, "Auto Can", "Canceled", "Completed", "Open", "Partial", "Held");
    }

}
