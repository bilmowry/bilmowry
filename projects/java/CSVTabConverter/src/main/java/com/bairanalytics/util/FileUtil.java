package com.bairanalytics.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: bil
 * 
 * 
 * To change this template use File | Settings | File Templates.
 */
public class FileUtil {
    /**
     * Read a file into a list.
     *
     * @param file
     * @return
     */
    public static List<String> readFile(final File file) {
        List<String> list = new ArrayList<String>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String text = null;
            while ((text = reader.readLine()) != null) {
                list.add(text);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (Exception e) {
            }
        }
        return list;
    }

    /**
     * Reverses a list.
     *
     * @param list
     * @return
     */
    public static List<String> reverseList(final List<String> list) {
        Collections.reverse(list);
        return list;
    }

    /**
     * Swap out delimiters in a list taken frm delimited data.  I.e. comma for tab.
     *
     * @param list
     * @param inDelimiter
     * @param outDelimiter
     * @return
     */
    public static List<String> swapDelimiter(final List<String> list, final String inDelimiter, final String outDelimiter) {
        List<String> out = new ArrayList<String>();
        for (String line : list) {
            line = line.replaceAll(inDelimiter, outDelimiter);
            out.add(line);
        }
        return out;
    }

    /**
     * Write a file to project (default) directory.
     *
     * @param outputFileName
     * @param list
     * @return
     */
    public static File writeFile(final String outputFileName, final List<String> list) {
        File outputFile = null;
        FileWriter writer = null;
        try {
            outputFile = new File(outputFileName);
            writer = new FileWriter(outputFile);
            for (String line : list) {
                writer.write(line);
                writer.write("\r\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (Exception e) {
            }
        }
        return outputFile;
    }

    /**
     * Retrieve a file as a list and print it to the console.
     *
     * @param file
     */
    public static void printFile(final File file) {
        List<String> list = readFile(file);
        for (String line : list) {
            System.out.println(line);
        }
    }
}
