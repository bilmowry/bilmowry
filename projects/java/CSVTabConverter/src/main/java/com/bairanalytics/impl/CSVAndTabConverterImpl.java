package com.bairanalytics.impl;

import com.bairanalytics.CSVAndTabConverter;
import com.bairanalytics.util.FileUtil;

import java.io.*;
import java.util.Calendar;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: bil
 * 
 * 
 */
public class CSVAndTabConverterImpl implements CSVAndTabConverter {

    public File convertFromTabToCSV(File inputFile) {
        List<String> read = FileUtil.readFile(inputFile);
        List<String> reverse = FileUtil.reverseList(read);
        List<String> swap = FileUtil.swapDelimiter(reverse, "[\t]", ",");
        String outputFileName = Calendar.getInstance().getTimeInMillis() + inputFile.getName() + ".csv";
        return FileUtil.writeFile(outputFileName, swap);
    }

    public File convertFromCSVToTab(File inputFile) {
        List<String> read = FileUtil.readFile(inputFile);
        List<String> reverse = FileUtil.reverseList(read);
        List<String> swap = FileUtil.swapDelimiter(reverse, "[,]", "\t");
        String outputFileName = Calendar.getInstance().getTimeInMillis() + inputFile.getName() + ".txt";
        return FileUtil.writeFile(outputFileName, swap);
    }
}

