package com.bairanalytics;

import java.io.File;

public interface CSVAndTabConverter {
    /**
      * This method takes a Tab( "\t" ) separated file as an argument. The tab file can
      * contain any number of columns but all lines will contain to same number
      * of columns. The method will return a CSV (",") file where all tabs have
      * been replaced by commas with the line order reversed.
      *
      * Assume: No tabs are contained in the data.
      *
      * @param inputFile
      *            The tab file.
      * @return A File of with the same data in CSV format with the line order reversed.
      */
    public File convertFromTabToCSV(File inputFile);
    /**
      * This method takes a Comma( "," ) separated  file as an argument. The comma file can
      * contain any number of columns but all lines will contain to same number
      * of columns. The method will return a Tab ("\t") file where all commas have
      * been replaced by tabs with the line order reversed.
      *
      * Assume: No commas are contained in the data.
      *
      * @param inputFile
      *            The CSV file.
      * @return A File of with the same data in Tab format with the line order reversed.
      */
    public File convertFromCSVToTab(File inputFile);

}