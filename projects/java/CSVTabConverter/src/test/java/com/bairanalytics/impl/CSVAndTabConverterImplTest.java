package com.bairanalytics.impl;

import com.bairanalytics.CSVAndTabConverter;
import com.bairanalytics.util.FileUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URL;

/**
 * CSVAndTabConverterImpl Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>Jul 29, 2014</pre>
 */
public class CSVAndTabConverterImplTest {

    private CSVAndTabConverter csvAndTabConverter = new CSVAndTabConverterImpl();

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {
        System.out.println("---------------------------------------------------");
    }

    /**
     * Method: convertFromTabToCSV(File inputFile)
     */
    @Test
    public void testConvertFromTabToCSV() throws Exception {
        URL dir_url = ClassLoader.getSystemResource("bears13preseason.txt");
        File input = new File(dir_url.toURI());
        File output = csvAndTabConverter.convertFromTabToCSV(input);

        assertTrue(null != output);

        System.out.println("Input file " + input.getName());
        FileUtil.printFile(input);
        System.out.println("Output file " + output.getName());
        FileUtil.printFile(output);
    }

    /**
     * Method: convertFromCSVToTab(File inputFile)
     */
    @Test
    public void testConvertFromCSVToTab() throws Exception {
        URL dir_url = ClassLoader.getSystemResource("bears13preseason.csv");
        File input = new File(dir_url.toURI());
        File output = csvAndTabConverter.convertFromCSVToTab(input);

        assertTrue(null != output);

        System.out.println("Input file " + input.getName());
        FileUtil.printFile(input);
        System.out.println("Output file " + output.getName());
        FileUtil.printFile(output);
    }
} 
