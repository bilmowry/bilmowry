Code Submission from Bil Mowry July 30, 2013  bil.mowry@yahoo.com

From BAIR -- Coding Exercise: I have attached a file called CSVAndTabConverter.java. Please write a Java class that implements this interface. Feel free to contact John with questions about the exercise. Return the source code for the exercise by email to John. 

Instructions From the interface:

    /**
      * This method takes a Tab( "\t" ) separated file as an argument. The tab file can
      * contain any number of columns but all lines will contain to same number
      * of columns. The method will return a CSV (",") file where all tabs have
      * been replaced by commas with the line order reversed.
      *
      * Assume: No tabs are contained in the data.
      *
      * @param inputFile
      *            The tab file.
      * @return A File of with the same data in CSV format with the line order reversed.
      */
    public File convertFromTabToCSV(File inputFile);
    /**
      * This method takes a Comma( "," ) separated  file as an argument. The comma file can
      * contain any number of columns but all lines will contain to same number
      * of columns. The method will return a Tab ("\t") file where all commas have
      * been replaced by tabs with the line order reversed.
      *
      * Assume: No commas are contained in the data.
      *
      * @param inputFile
      *            The CSV file.
      * @return A File of with the same data in Tab format with the line order reversed.
      */
    public File convertFromCSVToTab(File inputFile);

My INterpretation of the task:

-Write a program based on the interface provides that reads a file, changes either tab delimiters to commas or vice versa, reverses the line order in the file (bottom line at top, etc.) and writes the result out to a new file.  The data files are located in src->main->resources

How to execute this program:
This program is a Maven application that runs via unit tests.  In the folder with pom.xml run:

mvn clean install

Expected results:
There will be output in the console that shows the results, and two files with timestamps in the main directory that are the converted files.

Requirements:
--JDK 7 (might work with 6)
--Maven 3.0.x
--(This could also be imported into an IDE and run, but I have not included the IntelliJ files used in this)

