package com.menards.pos.ws.common.util;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class GMTDateDeserializer extends JsonDeserializer<Date> {

    @SuppressWarnings("unused")
    private final Logger log = LoggerFactory.getLogger(GMTDateDeserializer.class);

    /*
     * Receive GMT milliseconds from UI, truncate timestamp, use same date but cast as this timezome
     * for internal handling (all times in the app are local).  
     * 
     * Writing to Progress truncates time and stores no zone, so we want to store and use them as "UTC/GMT".
     * 
     * pretty much ignore timezone once inside the application; it's useless
     */
    @Override
    public Date deserialize(JsonParser arg0, DeserializationContext arg1) throws IOException, JsonProcessingException {
	
	return GMTDateUtil.getDate(arg0.getLongValue(), GMTDateUtil.GMT, GMTDateUtil.DEFAULT);
    }
}