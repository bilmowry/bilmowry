package com.menards.pos.ws.common.util;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class GMTDateSerializer extends JsonSerializer<Date> {
    
    @SuppressWarnings("unused")
    private final Logger log = LoggerFactory.getLogger(GMTDateSerializer.class);
    
    /*
     * Send back GMT milliseconds to UI
     */
    @Override
    public void serialize(Date aDate, JsonGenerator aJsonGenerator, SerializerProvider aSerializerProvider)
	    throws IOException, JsonProcessingException {
	
	aJsonGenerator.writeNumber(GMTDateUtil.getMillis(aDate.getTime(), GMTDateUtil.DEFAULT, GMTDateUtil.GMT));		
    }

}
