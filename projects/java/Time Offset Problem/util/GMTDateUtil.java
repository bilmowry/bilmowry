package com.menards.pos.ws.common.util;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.time.DateUtils;

public class GMTDateUtil {

    public static final TimeZone GMT = TimeZone.getTimeZone("GMT");
    public static final TimeZone DEFAULT = TimeZone.getDefault();
    
    public static Long getMillis(Long timeValue, TimeZone inZone, TimeZone outZone) {
	return DateUtils.round(calculateTruncatedDate(timeValue, inZone, outZone),Calendar.DAY_OF_MONTH).getTimeInMillis();
    }
    
    public static Date getDate(Long timeValue, TimeZone inZone, TimeZone outZone) {
	return DateUtils.round(calculateTruncatedDate(timeValue, inZone, outZone),Calendar.DAY_OF_MONTH).getTime();
    }   
    
    private static Calendar calculateTruncatedDate(Long timeValue, TimeZone inZone, TimeZone outZone) {
	Calendar inDate = Calendar.getInstance(inZone);
	inDate.setTimeInMillis(timeValue);
	Integer year = inDate.get(Calendar.YEAR);
	Integer month = inDate.get(Calendar.MONTH);
	Integer day = inDate.get(Calendar.DAY_OF_MONTH);
	Calendar outCal = Calendar.getInstance(outZone);
	outCal.set(year, month, day, 0, 0, 0);
	return outCal;
    }

}
