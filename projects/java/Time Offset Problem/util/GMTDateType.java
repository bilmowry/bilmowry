package com.menards.pos.ws.common.util;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;
import org.springframework.util.ObjectUtils;


public class GMTDateType implements UserType {

    private static final int[] SQL_TYPES = { Types.DATE };

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor arg2, Object arg3)
	    throws HibernateException, SQLException {
	Date dbDate = rs.getDate(names[0]);
	if (rs.wasNull()) {
	    return null;
	}
	// date in DB are GMT, convert to Local without timestamp
	return GMTDateUtil.getDate(dbDate.getTime(), GMTDateUtil.GMT, GMTDateUtil.DEFAULT);
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor arg3)
	    throws HibernateException, SQLException {
	if (value == null) {
	    st.setNull(index, Types.DATE);
	} else {
	    Date appDate = (Date) value;
	    st.setTimestamp(index,new Timestamp(GMTDateUtil.getMillis(appDate.getTime(), GMTDateUtil.GMT, GMTDateUtil.DEFAULT)));
	}
    }

    @Override
    public Object deepCopy(Object arg0) throws HibernateException {
	return arg0;
    }

    @Override
    public Object assemble(Serializable arg0, Object arg1) throws HibernateException {
	return arg0;
    }

    @Override
    public Serializable disassemble(Object arg0) throws HibernateException {
	return (Serializable) arg0;

    }

    @Override
    public boolean equals(Object arg0, Object arg1) throws HibernateException {
	return ObjectUtils.nullSafeEquals(arg0, arg1);
    }

    @Override
    public int hashCode(Object arg0) throws HibernateException {
	if (arg0 != null)
	    return arg0.hashCode();
	else
	    return 0;
    }

    @Override
    public boolean isMutable() {
	return false;
    }

    @Override
    public Object replace(Object arg0, Object arg1, Object arg2) throws HibernateException {
	return arg0;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Class returnedClass() {
	return Date.class;
    }

    @Override
    public int[] sqlTypes() {
	return SQL_TYPES;
    }

}
