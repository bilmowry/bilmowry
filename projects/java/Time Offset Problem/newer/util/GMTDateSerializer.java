package com.menards.pos.ws.common.util;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class GMTDateSerializer extends JsonSerializer<Date> {
    
    @SuppressWarnings("unused")
    private final Logger log = LoggerFactory.getLogger(GMTDateSerializer.class);
    
    /*
     * Send back GMT milliseconds to UI
     */
    @Override
    public void serialize(Date aDate, JsonGenerator aJsonGenerator, SerializerProvider aSerializerProvider)
	    throws IOException, JsonProcessingException {
	
	Calendar inCal = Calendar.getInstance(TimeZone.getDefault());
	inCal.setTimeInMillis(aDate.getTime());
	Integer year = inCal.get(Calendar.YEAR);
	Integer month = inCal.get(Calendar.MONTH);
	Integer day = inCal.get(Calendar.DAY_OF_MONTH);
	Calendar outCal = Calendar.getInstance(TimeZone.getTimeZone(("GMT")));
	outCal.set(year, month, day, 0, 0, 0);

	aJsonGenerator.writeNumber(outCal.getTimeInMillis());		
    }

}
