package com.menards.pos.ws.common.util;

import org.apache.commons.lang.StringUtils;

public class FieldSplitter {
    
    public static String getSplit(String list, String delimiter, int index) {
	String[] choices = StringUtils.split(list, delimiter);
	if (null != choices) {
	    return null != choices[1] ? choices[index] : "";
	}
	return "";
    }

}
