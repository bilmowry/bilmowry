package com.menards.pos.ws.common.domain.entity.menard;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.menards.pos.ws.common.domain.TableNames;
import com.menards.pos.ws.common.domain.entity.EntityBase;
import com.menards.pos.ws.common.util.GMTDateType;

@Entity
@TypeDefs({
	@TypeDef(name="GMTDateType", typeClass=GMTDateType.class)
})
@Table(name = TableNames.PALLET_HEADER)
public class PalletHeaderEntity extends EntityBase {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PalletID")
    private Integer palletId;

    @Column(name = "PalletDescriptionTxt")
    private String palletDescriptionTxt;

    @Column(name = "DepartmentNbr")
    private Integer departmentNumber;

    @Column(name = "CreateDate")
//    @Type(type="GMTDateType")
    private Date createDate;

    @Column(name = "LastActivityDate")
//    @Type(type="GMTDateType")
    private Date lastActivityDate;

    @Column(name = "TeamMemberName")
    private String teamMemberName;

    @Column(name = "BulkFlag")
    private Boolean bulkFlag;

    @Column(name = "SeasonalTxt")
    private String seasonalTxt;

    @Column(name = "LocationID")
    private String locationId;

    @Column(name = "PromoEndDate")
//    @Type(type="GMTDateType")
    private Date promoEndDate;

    @Column(name = "PalletDeletedFlag")
    private Boolean palletDeletedFlag;

    @Column(name = "PullByDeptNbr")
    private Integer pullByDeptNbr;

    @OneToMany(mappedBy = "palletHeaderEntity", fetch=FetchType.EAGER)
    @Cascade({CascadeType.ALL})
    private List<PalletDetailEntity> palletDetailEntities;

    public PalletHeaderEntity() {
	// no code
    }

    public final Integer getPalletId() {
	return palletId;
    }

    public final void setPalletId(Integer palletId) {
	this.palletId = palletId;
    }

    public final String getPalletDescriptionTxt() {
	return palletDescriptionTxt;
    }

    public final void setPalletDescriptionTxt(String palletDescriptionTxt) {
	this.palletDescriptionTxt = palletDescriptionTxt;
    }

    public final Integer getDepartmentNumber() {
	return departmentNumber;
    }

    public final void setDepartmentNumber(Integer departmentNumber) {
	this.departmentNumber = departmentNumber;
    }

    public final Date getCreateDate() {
	return createDate;
    }

    public final void setCreateDate(Date createDate) {
	this.createDate = createDate;
    }

    public final Date getLastActivityDate() {
	return lastActivityDate;
    }

    public final void setLastActivityDate(Date lastActivityDate) {
	this.lastActivityDate = lastActivityDate;
    }

    public final String getTeamMemberName() {
	return teamMemberName;
    }

    public final void setTeamMemberName(String teamMemberName) {
	this.teamMemberName = teamMemberName;
    }

    public final Boolean getBulkFlag() {
	return bulkFlag;
    }

    public final void setBulkFlag(Boolean bulkFlag) {
	this.bulkFlag = bulkFlag;
    }

    public final String getSeasonalTxt() {
	return seasonalTxt;
    }

    public final void setSeasonalTxt(String seasonalTxt) {
	this.seasonalTxt = seasonalTxt;
    }

    public final String getLocationId() {
	return locationId;
    }

    public final void setLocationId(String locationId) {
	this.locationId = locationId;
    }

    public final Date getPromoEndDate() {
	return promoEndDate;
    }

    public final void setPromoEndDate(Date promoEndDate) {
	this.promoEndDate = promoEndDate;
    }

    public final List<PalletDetailEntity> getPalletDetailEntities() {
	return palletDetailEntities;
    }

    public final void setPalletDetailEntities(List<PalletDetailEntity> palletDetailEntities) {
	this.palletDetailEntities = palletDetailEntities;
    }

    public Boolean getPalletDeletedFlag() {
	return palletDeletedFlag;
    }

    public void setPalletDeletedFlag(Boolean palletDeletedFlag) {
	this.palletDeletedFlag = palletDeletedFlag;
    }

    public Integer getPullByDeptNbr() {
	return pullByDeptNbr;
    }

    public void setPullByDeptNbr(Integer pullByDeptNbr) {
	this.pullByDeptNbr = pullByDeptNbr;
    }
}