package com.menards.pos.ws.common.domain.entity.json.overstock;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.menards.pos.ws.common.util.GMTDateDeserializer;
import com.menards.pos.ws.common.util.GMTDateSerializer;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonTypeName("palletHeader")
public class PalletHeader
{
	@JsonProperty("palletId")
	private Integer palletId;

	@JsonProperty("locationText")
	private String locationId;

	@JsonProperty("creationDate")
	@JsonSerialize(using=GMTDateSerializer.class)
//	@JsonDeserialize(using=GMTDateDeserializer.class)
	private Date createDate;

	@JsonProperty("seasonalType")
	private String seasonalTxt;

	@JsonProperty("promoDate")
	@JsonSerialize(using=GMTDateSerializer.class)
//	@JsonDeserialize(using=GMTDateDeserializer.class)
	private Date promoEndDate;

	@JsonProperty("bulkFlag")
	private Boolean bulkFlag;

	@JsonProperty("departmentNumber")
	private Integer departmentNumber;

	@JsonProperty("teamMemberName")
	private String teamMemberName;

	@JsonProperty("palletDescriptionTxt")
	private String palletDescriptionTxt;

	@JsonProperty("lastActivityDate")
	@JsonSerialize(using=GMTDateSerializer.class)
//	@JsonDeserialize(using=GMTDateDeserializer.class)
	private Date lastActivityDate;

	@JsonProperty("palletDetails")
	private List<PalletDetail> palletDetails;

	public PalletHeader(Integer palletId, String locationId, Date createDate, String seasonalTxt, Date promoEndDate, Boolean bulkFlag, Integer departmentNumber, String teamMemberName, String palletDescriptionTxt, Date lastActivityDate, List<PalletDetail> palletDetails) // NOPMD
	{
		this.palletId = palletId;
		this.locationId = locationId;
		this.seasonalTxt = seasonalTxt;
		this.createDate = createDate;
		this.lastActivityDate = lastActivityDate;
		this.promoEndDate = promoEndDate;
		this.bulkFlag = bulkFlag;
		this.departmentNumber = departmentNumber;
		this.teamMemberName = teamMemberName;
		this.palletDescriptionTxt = palletDescriptionTxt;
		this.palletDetails = palletDetails;
	}

	public PalletHeader()
	{
		// no-arg constructor for Jackson
	}

	public Integer getPalletId()
	{
		return palletId;
	}

	public void setPalletId(Integer palletId)
	{
		this.palletId = palletId;
	}

	public String getLocationId()
	{
		return locationId;
	}

	public void setLocationId(String locationId)
	{
		this.locationId = locationId;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public String getSeasonalTxt()
	{
		return seasonalTxt;
	}

	public void setSeasonalTxt(String seasonalTxt)
	{
		this.seasonalTxt = seasonalTxt;
	}

	public Date getPromoEndDate()
	{
		return promoEndDate;
	}

	public void setPromoEndDate(Date promoEndDate)
	{
		this.promoEndDate = promoEndDate;
	}

	public Boolean getBulkFlag()
	{
		return bulkFlag;
	}

	public void setBulkFlag(Boolean bulkFlag)
	{
		this.bulkFlag = bulkFlag;
	}

	public Integer getDepartmentNumber()
	{
		return departmentNumber;
	}

	public void setDepartmentNumber(Integer departmentNumber)
	{
		this.departmentNumber = departmentNumber;
	}

	public String getTeamMemberName()
	{
		return teamMemberName;
	}

	public void setTeamMemberName(String teamMemberName)
	{
		this.teamMemberName = teamMemberName;
	}

	public String getPalletDescriptionTxt()
	{
		return palletDescriptionTxt;
	}

	public void setPalletDescriptionTxt(String palletDescriptionTxt)
	{
		this.palletDescriptionTxt = palletDescriptionTxt;
	}

	public Date getLastActivityDate()
	{
		return lastActivityDate;
	}

	public void setLastActivityDate(Date lastActivityDate)
	{
		this.lastActivityDate = lastActivityDate;
	}

	public List<PalletDetail> getPalletDetails()
	{
		return palletDetails;
	}

	public void setPalletDetails(List<PalletDetail> palletDetails)
	{
		this.palletDetails = palletDetails;
	}
}