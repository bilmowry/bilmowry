package com.menards.pos.ws.common.service.overstock;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.menards.pos.ws.common.domain.dao.menard.PalletHistoryRepository;
import com.menards.pos.ws.common.domain.entity.json.overstock.PalletChangeType;
import com.menards.pos.ws.common.domain.entity.menard.PalletHeaderEntity;
import com.menards.pos.ws.common.domain.entity.menard.PalletHistoryEntity;
import com.menards.pos.ws.common.domain.value.Decimal;

/**
 * Service used to deal with the PalletHistory table
 * 
 * @author alandsve
 */
@Service
public class PalletHistoryService
{
	private final Logger log = LoggerFactory.getLogger(PalletHistoryService.class);

	@Autowired
	private PalletHistoryRepository palletHistoryRepo;

	/**
	 * Logs a pallet detail change to the PalletHistory table
	 * 
	 * @param palletHdr PalletHeaderEntity - Header record of the detail that changed
	 * @param sku Integer - sku that changed
	 * @param teamMemberName String - TM that made the change
	 * @param type PalletChangeType - type of change
	 * @param beforeQty Decimal - quantity before the change
	 * @param afterQty Decimal - quantity after the change
	 */
	public void logPalletDetailChange(final PalletHeaderEntity palletHdr, final Integer sku, final String teamMemberName, final PalletChangeType type, final Decimal beforeQty, final Decimal afterQty)
	{
		try
		{
			final Date now = new Date();
			final String nowTime = new SimpleDateFormat("HH:mm:ss").format(now);

			final PalletHistoryEntity entity = new PalletHistoryEntity();
			entity.setPalletId(palletHdr.getPalletId());
			entity.setActivityDate(now);
			entity.setAfterQuantity(afterQty);
			entity.setBeforeQuantity(beforeQty);
			entity.setWhoChangeTxt(teamMemberName);
			entity.setTimeChangeTxt(nowTime);
			entity.setChangeTypeTxt(type.getValue());
			entity.setSku(sku);

			//TODO finish implementing
			switch (type)
			{
				case ADD_SKU:
					break;
				case DESC_CHG:
					break;
				case LOC_CHG:
					break;
				case QTY_CHG:
					break;
				case REMOVE_SKU:
					entity.setEventLogTxt("REMOVE |Sku:" + sku + " |Qty:" + beforeQty + " |Loc:" + palletHdr.getLocationId() + " |Desc: " + palletHdr.getPalletDescriptionTxt());
					break;
				case RESTORE_SKU:
					entity.setEventLogTxt("RESTORE |Sku:" + sku + " |Qty:" + afterQty + " |Loc:" + palletHdr.getLocationId() + " |Desc: " + palletHdr.getPalletDescriptionTxt());
					break;
				case SEASON_CHG:
					break;
			}

			palletHistoryRepo.saveAndFlush(entity);
		}
		catch (DataAccessException dae)
		{
			log.error("Error saving PalletHistoryEntity", dae);
		}
	}
}