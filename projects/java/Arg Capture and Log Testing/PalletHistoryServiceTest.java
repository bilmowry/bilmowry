package com.menards.pos.ws.common.service.overstock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.RecoverableDataAccessException;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

import com.menards.pos.ws.common.domain.dao.menard.PalletHistoryRepository;
import com.menards.pos.ws.common.domain.entity.json.overstock.PalletChangeType;
import com.menards.pos.ws.common.domain.entity.menard.PalletHeaderEntity;
import com.menards.pos.ws.common.domain.entity.menard.PalletHistoryEntity;
import com.menards.pos.ws.common.domain.value.Decimal;
import com.menards.pos.ws.common.test.AbstractPojoTest;

public class PalletHistoryServiceTest extends AbstractPojoTest {
    private static Logger logger = LoggerFactory.getLogger(PalletHistoryServiceTest.class);
    
    @InjectMocks
    private PalletHistoryService palletHistoryService;

    @Mock
    private PalletHistoryRepository mockPalletHistoryRepository;

    @Captor
    private ArgumentCaptor<PalletHistoryEntity> history;

    private PalletHeaderEntity palletHdr;

    @Before
    public void setUp() throws Exception {
	MockitoAnnotations.initMocks(this);
	palletHdr = this.generatePojo(PalletHeaderEntity.class);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testAddSku() {
	palletHistoryService.logPalletDetailChange(palletHdr, 4140, "Charlie the Unicorn", PalletChangeType.ADD_SKU,
		Decimal.TEN, Decimal.ONE);
	verify(mockPalletHistoryRepository).saveAndFlush(history.capture());
	assertEquals(null, history.getValue().getEventLogTxt());
    }

    @Test
    public void testDescChg() {
	palletHistoryService.logPalletDetailChange(palletHdr, 4140, "Charlie the Unicorn", PalletChangeType.DESC_CHG,
		Decimal.TEN, Decimal.ONE);
	verify(mockPalletHistoryRepository).saveAndFlush(history.capture());
	assertEquals(null, history.getValue().getEventLogTxt());
    }

    @Test
    public void testLocChg() {
	palletHistoryService.logPalletDetailChange(palletHdr, 4140, "Charlie the Unicorn", PalletChangeType.LOC_CHG,
		Decimal.TEN, Decimal.ONE);
	verify(mockPalletHistoryRepository).saveAndFlush(history.capture());
	assertEquals(null, history.getValue().getEventLogTxt());
    }

    @Test
    public void testQtyChg() {
	palletHistoryService.logPalletDetailChange(palletHdr, 4140, "Charlie the Unicorn", PalletChangeType.QTY_CHG,
		Decimal.TEN, Decimal.ONE);
	verify(mockPalletHistoryRepository).saveAndFlush(history.capture());
	assertEquals(null, history.getValue().getEventLogTxt());
    }

    @Test
    public void testRemoveSku() {
	palletHistoryService.logPalletDetailChange(palletHdr, 4140, "Charlie the Unicorn", PalletChangeType.REMOVE_SKU,
		Decimal.TEN, Decimal.ONE);
	verify(mockPalletHistoryRepository).saveAndFlush(history.capture());
	assertEquals("REMOVE |Sku:4140 |Qty:10 |Loc:foo |Desc: foo", history.getValue().getEventLogTxt());
    }

    @Test
    public void testRestoreSku() {
	palletHistoryService.logPalletDetailChange(palletHdr, 4140, "Charlie the Unicorn",
		PalletChangeType.RESTORE_SKU, Decimal.TEN, Decimal.ONE);
	verify(mockPalletHistoryRepository).saveAndFlush(history.capture());
	assertEquals("RESTORE |Sku:4140 |Qty:1 |Loc:foo |Desc: foo", history.getValue().getEventLogTxt());
    }

    @Test
    public void testSeasonChg() {
	palletHistoryService.logPalletDetailChange(palletHdr, 4140, "Charlie the Unicorn", PalletChangeType.SEASON_CHG,
		Decimal.TEN, Decimal.ONE);
	verify(mockPalletHistoryRepository).saveAndFlush(history.capture());
	assertEquals(null, history.getValue().getEventLogTxt());
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Test
    public void testError() {
	ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory
		.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
	final Appender mockAppender = mock(Appender.class);
	when(mockAppender.getName()).thenReturn("MOCK");
	root.addAppender(mockAppender);

	doThrow(new RecoverableDataAccessException("data error")).when(mockPalletHistoryRepository).saveAndFlush(
		(PalletHistoryEntity) anyObject());
	palletHistoryService.logPalletDetailChange(palletHdr, 4140, "Charlie the Unicorn", PalletChangeType.SEASON_CHG,
		Decimal.TEN, Decimal.ONE);

	verify(mockAppender).doAppend(argThat(new ArgumentMatcher() {
	    @Override
	    public boolean matches(final Object argument) {
		return ((LoggingEvent) argument).getFormattedMessage()
			.contains("Error saving PalletHistoryEntity");
	    }
	}));
    }

}
