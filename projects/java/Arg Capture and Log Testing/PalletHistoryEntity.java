package com.menards.pos.ws.common.domain.entity.menard;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.menards.pos.ws.common.domain.TableNames;
import com.menards.pos.ws.common.domain.entity.EntityBase;
import com.menards.pos.ws.common.domain.factory.DecimalUserType;
import com.menards.pos.ws.common.domain.value.Decimal;

@Entity
@IdClass(PalletHistoryEntity.class)
@Table(name = TableNames.PALLET_HISTORY)
public class PalletHistoryEntity extends EntityBase
{
	private static final long serialVersionUID = 1L;

	@Column(name = "PalletID")
	private Integer palletId;

	@Column(name = "ActivityDate")
	private Date activityDate;

	@Column(name = "EventLogTxt")
	private String eventLogTxt;

	@Id
	@Column(name = "SKU")
	private Integer sku;

	@Id
	@Column(name = "TimeChgTxt")
	private String timeChangeTxt;

	@Id
	@Column(name = "WhoChgTxt")
	private String whoChangeTxt;

	@Id
	@Column(name = "ChgTypeTxt")
	private String changeTypeTxt;

	@Column(name = "BeforeQty")
	@Type(type = DecimalUserType.TYPE_VALUE)
	private Decimal beforeQuantity;

	@Column(name = "AfterQty")
	@Type(type = DecimalUserType.TYPE_VALUE)
	private Decimal afterQuantity;

	public PalletHistoryEntity()
	{
		// no code
	}

	public PalletHistoryEntity(Integer palletId, Date activityDate, String eventLogTxt, Integer sku, String timeChangeTxt, String whoChangeTxt, String changeTypeTxt, Decimal beforeQuantity, Decimal afterQuantity) // NOPMD
	{
		super();
		this.palletId = palletId;
		this.activityDate = activityDate;
		this.eventLogTxt = eventLogTxt;
		this.sku = sku;
		this.timeChangeTxt = timeChangeTxt;
		this.whoChangeTxt = whoChangeTxt;
		this.changeTypeTxt = changeTypeTxt;
		this.beforeQuantity = beforeQuantity;
		this.afterQuantity = afterQuantity;
	}

	public Integer getPalletId()
	{
		return palletId;
	}

	public void setPalletId(Integer palletId)
	{
		this.palletId = palletId;
	}

	public Date getActivityDate()
	{
		return activityDate;
	}

	public void setActivityDate(Date activityDate)
	{
		this.activityDate = activityDate;
	}

	public String getEventLogTxt()
	{
		return eventLogTxt;
	}

	public void setEventLogTxt(String eventLogTxt)
	{
		this.eventLogTxt = eventLogTxt;
	}

	public Integer getSku()
	{
		return sku;
	}

	public void setSku(Integer sku)
	{
		this.sku = sku;
	}

	public String getTimeChangeTxt()
	{
		return timeChangeTxt;
	}

	public void setTimeChangeTxt(String timeChangeTxt)
	{
		this.timeChangeTxt = timeChangeTxt;
	}

	public String getWhoChangeTxt()
	{
		return whoChangeTxt;
	}

	public void setWhoChangeTxt(String whoChangeTxt)
	{
		this.whoChangeTxt = whoChangeTxt;
	}

	public String getChangeTypeTxt()
	{
		return changeTypeTxt;
	}

	public void setChangeTypeTxt(String changeTypeTxt)
	{
		this.changeTypeTxt = changeTypeTxt;
	}

	public Decimal getBeforeQuantity()
	{
		return beforeQuantity;
	}

	public void setBeforeQuantity(Decimal beforeQuantity)
	{
		this.beforeQuantity = beforeQuantity;
	}

	public Decimal getAfterQuantity()
	{
		return afterQuantity;
	}

	public void setAfterQuantity(Decimal afterQuantity)
	{
		this.afterQuantity = afterQuantity;
	}
}