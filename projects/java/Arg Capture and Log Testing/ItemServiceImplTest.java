package com.menards.pos.ws.common.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.LoggerFactory;
import org.springframework.dao.RecoverableDataAccessException;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

import com.menards.pos.ws.common.domain.dao.handy.ItemPluRepository;
import com.menards.pos.ws.common.domain.dao.handy.ItemReplenRepository;
import com.menards.pos.ws.common.domain.dao.menard.ItemInfoRepository;
import com.menards.pos.ws.common.domain.entity.handy.ItemPluEntity;
import com.menards.pos.ws.common.domain.entity.handy.ItemReplenEntity;
import com.menards.pos.ws.common.domain.entity.menard.ItemInfoEntity;
import com.menards.pos.ws.common.domain.value.Decimal;
import com.menards.pos.ws.common.service.ItemService;
import com.menards.pos.ws.common.test.AbstractPojoTest;

public class ItemServiceImplTest extends AbstractPojoTest {

    @InjectMocks
    ItemService itemService = new ItemServiceImpl();

    @Mock
    private ItemReplenRepository mockItemReplenRepo;

    @Mock
    private ItemPluRepository mockItemPluRepo;

    @Mock
    private ItemInfoRepository mockItemInfoRepo;

    private ItemReplenEntity replen;

    private ItemPluEntity plu;

    private ItemInfoEntity info;

    @Before
    public void setUp() throws Exception {
	MockitoAnnotations.initMocks(this);
	replen = generatePojo(ItemReplenEntity.class);
	plu = generatePojo(ItemPluEntity.class);
	info = generatePojo(ItemInfoEntity.class);

	when(mockItemReplenRepo.findBySku(anyInt())).thenReturn(replen);
	when(mockItemInfoRepo.findBySku(anyInt())).thenReturn(info);
	when(mockItemPluRepo.findBySku(anyInt())).thenReturn(plu);
    }

    @After
    public void tearDown() throws Exception {
	replen = null;
	plu = null;
	info = null;
    }

    @Test
    public void testGetQtyOnHand() {
	assertEquals(AbstractPojoTest.USER_DECIMAL, itemService.getQtyOnHand(123));
    }

    @Test
    public void testGetQtyOnHandNull() {
	when(mockItemReplenRepo.findBySku(anyInt())).thenReturn(null);
	assertNull(itemService.getQtyOnHand(123));
    }

    @Test
    public void testGetOverstockQty() {
	assertEquals(AbstractPojoTest.USER_DECIMAL, itemService.getOverstockQty(123));
    }

    @Test
    public void testGetOverstockQtyNull() {
	when(mockItemInfoRepo.findBySku(anyInt())).thenReturn(null);
	assertNull(itemService.getOverstockQty(123));
    }

    @Test
    public void testGetSkuDescription() {
	assertEquals(AbstractPojoTest.STRING, itemService.getSkuDescription(123));
    }

    @Test
    public void testGetSkuDescriptionNull() {
	when(mockItemPluRepo.findBySku(anyInt())).thenReturn(null);
	assertNull(itemService.getSkuDescription(123));
    }

    @Test
    public void testGetItemInfo() {
	ItemInfoEntity test = itemService.getItemInfo(123);
	assertEquals(info.getInboundFreightCost(), test.getInboundFreightCost());
    }

    @Test
    public void testDecrementOverstockQuantity() {
	itemService.decrementOverstockQuantity(123, Decimal.ONE);
	assertEquals(AbstractPojoTest.USER_DECIMAL.subtract(Decimal.ONE), info.getOverstockQuantity());
    }

    @Test
    public void testDecrementOverstockQuantityNull() {
	when(mockItemInfoRepo.findBySku(anyInt())).thenReturn(null);
	itemService.decrementOverstockQuantity(123, Decimal.ONE);
	assertEquals(AbstractPojoTest.USER_DECIMAL, info.getOverstockQuantity());
    }

    @Test
    public void testDecrementOverstockQuantityZero() {
	itemService.decrementOverstockQuantity(123, new Decimal("25000"));
	assertEquals(Decimal.ZERO, info.getOverstockQuantity());
    }

    @Test
    public void testIncrementOverstockQuantity() {
	itemService.incrementOverstockQuantity(123, Decimal.TEN);
	assertEquals(AbstractPojoTest.USER_DECIMAL.add(Decimal.TEN), info.getOverstockQuantity());
    }

    @Test
    public void testIncrementOverstockQuantityNull() {
	when(mockItemInfoRepo.findBySku(anyInt())).thenReturn(null);
	itemService.incrementOverstockQuantity(123, Decimal.TEN);
	assertEquals(AbstractPojoTest.USER_DECIMAL, info.getOverstockQuantity());
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Test
    public void testUpdateOverstockQuantityException() {
	ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory
		.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
	final Appender mockAppender = mock(Appender.class);
	when(mockAppender.getName()).thenReturn("MOCK");
	root.addAppender(mockAppender);
	doThrow(new RecoverableDataAccessException("data error")).when(mockItemInfoRepo).saveAndFlush(
		(ItemInfoEntity) anyObject());
	
	itemService.incrementOverstockQuantity(123, Decimal.TEN);

	verify(mockAppender).doAppend(argThat(new ArgumentMatcher() {
	    @Override
	    public boolean matches(final Object argument) {
		return ((LoggingEvent) argument).getFormattedMessage().contains("Couldn't update overstock quantity");
	    }
	}));
    }

}
