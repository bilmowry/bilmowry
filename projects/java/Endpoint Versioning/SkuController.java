package com.menards.pos.ws.common.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.menards.pos.ws.common.domain.dto.entity.SkuDto;
import com.menards.pos.ws.common.domain.value.Decimal;
import com.menards.pos.ws.common.service.SkuService;

@Controller
@RequestMapping(value = "/sku/**", produces = MediaType.APPLICATION_JSON_VALUE)
public class SkuController {
	@Autowired
	private SkuService skuService;

	@RequestMapping(method = RequestMethod.GET, value = "/{sku}")
	public @ResponseBody
	SkuDto sku(@PathVariable Long sku) {
		return skuService.getDto(Decimal.valueOf(sku));
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/contract")
	public @ResponseBody
	String contract() {
		
		return skuService.contract();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/version")
	public @ResponseBody
	String version(@RequestHeader("Accept-Version") String version) {	
		
		return skuService.contract();
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/search/{search}")
	public @ResponseBody
	String search(@PathVariable String search) {	
		
		return skuService.search(search);
	}
	
}