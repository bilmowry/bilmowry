package com.menards.pos.ws.common.domain.dto;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

@SuppressWarnings("PMD.AbstractClassWithoutAbstractMethod")
public abstract class AbstractBaseMapper {
    
    private final Logger log = LoggerFactory.getLogger(AbstractBaseMapper.class);

    protected AbstractBaseMapper() {
	/* intentional */
    }

    public <T, O> T fromDTO(O fromDTO, Class<T> toEntity) {
	T newInstance = classFactory(toEntity);
	BeanUtils.copyProperties(fromDTO, newInstance);
	return newInstance;
    }

    public <T, O> T toDTO(O fromEntity, Class<T> toDTO) {
	T newInstance = classFactory(toDTO);
	BeanUtils.copyProperties(fromEntity, newInstance);
	return newInstance;
    }

    public <T, O> List<T> fromDTOs(List<O> fromDTOs, Class<T> toEntity) {
	List<T> tList = new ArrayList<T>();
	for (O o : fromDTOs) {
	    tList.add((T) toDTO(o, toEntity));
	}
	return tList;
    }

    public <T, O> List<T> toDTOs(List<O> fromEntities, Class<T> toDTO) {
	List<T> tList = new ArrayList<T>();
	for (O o : fromEntities) {
	    tList.add((T) toDTO(o, toDTO));
	}
	return tList;
    }

    protected <T> T classFactory(Class<T> clazz) {

	try {
	    return clazz.newInstance();
	} catch (InstantiationException e) {
	    log.error(e.getLocalizedMessage());
	} catch (IllegalAccessException e) {
	    log.error(e.getLocalizedMessage());
	}
	return null;

    }
}
