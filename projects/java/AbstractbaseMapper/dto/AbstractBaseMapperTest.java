package com.menards.pos.ws.common.domain.dto;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AbstractBaseMapperTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testFromDTO() {
	TestMapper testMapper = new TestMapperImpl();
	TestDto testDto = new TestDto(new Long(6));
	TestEntity testEntity = testMapper.fromDTO(testDto, TestEntity.class);

	assertEquals(testEntity.getId(), new Long(6));
    }

    @Test
    public void testToDTO() {
	TestMapper testMapper = new TestMapperImpl();
	TestEntity testEntity = new TestEntity(new Long(22));
	TestDto testDto = testMapper.toDTO(testEntity, TestDto.class);

	assertEquals(testDto.getId(), new Long(22));
    }

}