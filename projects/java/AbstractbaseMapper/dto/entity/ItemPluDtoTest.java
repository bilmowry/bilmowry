package com.menards.pos.ws.common.domain.dto.entity;

import java.text.ParseException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.menards.pos.ws.common.test.AbstractPojoTest;

public class ItemPluDtoTest extends AbstractPojoTest {
	
	@Before
	public void setUp() throws Exception {
	
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testProperties() throws ParseException {
		
		testPojo(ItemPluDto.class);

	}
	
}
