package com.menards.pos.ws.common.domain.dto.entity;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.menards.pos.ws.common.domain.value.Decimal;
import com.menards.pos.ws.common.test.AbstractPojoTest;

public class SkuDtoTest extends AbstractPojoTest {

    	private Decimal sku = new Decimal(34);
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testProperties() {
		
		testPojo(SkuDto.class);
		
		SkuDto skuDto = new SkuDto();
		skuDto.setSku(sku);
		skuDto.setSkuInvHistDto(new SkuInvHistDto());
		skuDto.setSkuMainDto(new SkuMainDto());
		skuDto.setSkuNotesDto(new SkuNotesDto());
		skuDto.setSkuOrderingDto(new SkuOrderingDto());
		skuDto.setSkuPPDDto(new SkuPPDDto());
		skuDto.setSkuPricingDto(new SkuPricingDto());
		skuDto.setSkuReturnsDto(new SkuReturnsDto());
		skuDto.setSkuSalesHistDto(new SkuSalesHistDto());
		
		assertNotNull(skuDto);
		assertNotNull(skuDto.getSkuInvHistDto());
		assertNotNull(skuDto.getSkuMainDto());
		assertNotNull(skuDto.getSkuNotesDto());
		assertNotNull(skuDto.getSkuOrderingDto());
		assertNotNull(skuDto.getSkuPPDDto());
		assertNotNull(skuDto.getSkuPricingDto());
		assertNotNull(skuDto.getSkuReturnsDto());
		assertNotNull(skuDto.getSkuSalesHistDto());
		
		

	}

}
