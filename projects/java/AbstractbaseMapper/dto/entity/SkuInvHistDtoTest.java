package com.menards.pos.ws.common.domain.dto.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.menards.pos.ws.common.test.AbstractPojoTest;

public class SkuInvHistDtoTest extends AbstractPojoTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testProperties() {
		
		testPojo(SkuInvHistDto.class);

	}

}
