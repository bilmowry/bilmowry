package com.menards.pos.ws.common.domain.dto;

public class TestDto {
	
	public TestDto() {}
	
	public TestDto(Long id) {
		this.setId(id);
	}
	
	private Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
