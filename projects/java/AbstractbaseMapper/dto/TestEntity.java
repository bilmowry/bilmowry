package com.menards.pos.ws.common.domain.dto;

public class TestEntity {
	
	public TestEntity(){}
	
	public TestEntity(Long id) {
		this.setId(id);
	}
	
	private Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}