package com.menards.pos.ws.common.domain.dto.mapper.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.menards.pos.ws.common.domain.dto.entity.SkuDto;
import com.menards.pos.ws.common.domain.dto.mapper.SkuMapper;
import com.menards.pos.ws.common.domain.entity.handy.ItemPluEntity;
import com.menards.pos.ws.common.domain.entity.handy.ItemReplenEntity;
import com.menards.pos.ws.common.domain.entity.menard.ItemInfoEntity;
import com.menards.pos.ws.common.domain.model.SkuEntity;
import com.menards.pos.ws.common.domain.value.Decimal;
import com.menards.pos.ws.common.test.AbstractPojoTest;

public class SkuMapperImplTest extends AbstractPojoTest {
    
    private SkuMapper skuMapper = new SkuMapperImpl();

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testNullEntity() {

	SkuDto skuDto = skuMapper.toDTO(null, SkuDto.class);

	assertTrue(null == skuDto.getSku());
	assertTrue(null == skuDto.getSkuMainDto());

    }

    @Test
    public void testToDTO() {

	SkuDto skuDto = skuMapper.toDTO(makeEntity(), SkuDto.class);

	assertEquals(skuDto.getSkuMainDto().getWarehsLoc(), "foo");

    }
    
    private SkuEntity makeEntity() {
	SkuEntity skuEntity = generatePojo(SkuEntity.class);
	skuEntity.setItemInfoEntity((ItemInfoEntity) generatePojo(ItemInfoEntity.class));
	skuEntity.setItemPluEntity((ItemPluEntity) generatePojo(ItemPluEntity.class));
	skuEntity.setItemReplenEntity((ItemReplenEntity) generatePojo(ItemReplenEntity.class));

	return skuEntity;
    }

}
