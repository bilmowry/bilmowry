package com.menards.pos.ws.common.domain.dto.mapper.impl;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.menards.pos.ws.common.domain.dto.entity.CouponDiscDto;
import com.menards.pos.ws.common.domain.dto.mapper.CouponDiscMapper;
import com.menards.pos.ws.common.domain.entity.handy.CouponDiscEntity;

public class CouponDiscMapperImplTest {
	
	CouponDiscMapper couponDiscMapper;
	
	CouponDiscEntity couponDiscEntity;
	CouponDiscDto couponDiscDto;
	
	Date testDate;

	@Before
	public void setUp() throws Exception {
		couponDiscMapper = new CouponDiscMapperImpl();
		couponDiscEntity = new CouponDiscEntity();
		couponDiscDto = new CouponDiscDto();
		testDate = makeADate("12-25-2025");
	}

	@After
	public void tearDown() throws Exception {
		couponDiscMapper = null;
		couponDiscEntity = null;
		couponDiscDto = null;
		testDate = null;
	}

	@Test
	public void testToDto() {
		populateEntity() ;
		couponDiscDto = couponDiscMapper.toDTO(couponDiscEntity, CouponDiscDto.class);
		
		assertEquals(couponDiscDto.getDescription(),"description");
		assertEquals(couponDiscDto.getDiscCode(),new Integer(1));
		assertEquals(couponDiscDto.getDiscType(),"discType");
		assertEquals(couponDiscDto.getEndDate(),testDate);

	}
	
	@Test
	public void testFromDto() {
		populateDto() ;
		couponDiscEntity = couponDiscMapper.fromDTO(couponDiscDto, CouponDiscEntity.class);
		
		assertEquals(couponDiscEntity.getDescription(),"desc2");
		assertEquals(couponDiscEntity.getDiscCode(),new Integer(4));
		assertEquals(couponDiscEntity.getDiscType(),"discType77");
		assertEquals(couponDiscEntity.getEndDate(),testDate);
	}
	
	private void populateEntity() {
		couponDiscEntity.setDescription("description");
		couponDiscEntity.setDiscCode(1);
		couponDiscEntity.setDiscType("discType");
		couponDiscEntity.setEndDate(testDate);
	}
	
	private void populateDto() {
		couponDiscDto.setDescription("desc2");
		couponDiscDto.setDiscCode(4);
		couponDiscDto.setDiscType("discType77");
		couponDiscDto.setEndDate(testDate);
	}
	
	private Date makeADate(String dateString) throws ParseException {	
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		return dateFormat.parse(dateString);
	}

}
