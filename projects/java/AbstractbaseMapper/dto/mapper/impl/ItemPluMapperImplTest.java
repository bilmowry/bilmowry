package com.menards.pos.ws.common.domain.dto.mapper.impl;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.menards.pos.ws.common.domain.dto.entity.ItemPluDto;
import com.menards.pos.ws.common.domain.dto.mapper.ItemPluMapper;
import com.menards.pos.ws.common.domain.entity.handy.ItemPluEntity;
import com.menards.pos.ws.common.domain.value.Decimal;

public class ItemPluMapperImplTest {

	ItemPluMapper itemPluMapper;
	
	private ItemPluEntity itemPluEntity;		
	private ItemPluDto itemPluDto;
	
	Date testDate;

	@Before
	public void setUp() throws Exception {
		itemPluMapper = new ItemPluMapperImpl();
		itemPluEntity = new ItemPluEntity();		
		itemPluDto = new ItemPluDto();
		testDate = makeADate("12-25-2025");
	}

	@After
	public void tearDown() throws Exception {
		itemPluMapper = null;
		itemPluEntity = null;
		itemPluDto = null;
	}

	@Test
	public void testToDto() {
		populateEntity() ;
		itemPluDto = itemPluMapper.toDTO(itemPluEntity, ItemPluDto.class);
		
		assertEquals(itemPluDto.getCloseoutStartDate(),testDate);
		assertEquals(itemPluDto.getDescription(),"description");
		assertEquals(itemPluDto.getDiscCode(),"discCode");
		assertEquals(itemPluDto.getSku(),new Decimal(12345));

	}
	
	@Test
	public void testFromDto() {
		populateDto() ;
		itemPluEntity = itemPluMapper.fromDTO(itemPluDto, ItemPluEntity.class);
				
		assertEquals(itemPluEntity.getCloseoutStartDate(),testDate);
		assertEquals(itemPluEntity.getDescription(),"description456");
		assertEquals(itemPluEntity.getDiscCode(),"discCode123");
		assertEquals(itemPluEntity.getSku(),new Decimal(67890));
	}
	
	private void populateEntity() {
		itemPluEntity.setCloseoutStartDate(testDate);
		itemPluEntity.setDescription("description");
		itemPluEntity.setDiscCode("discCode");
		itemPluEntity.setSku(new Decimal(12345));
	}
	
	private void populateDto() {
		itemPluDto.setCloseoutStartDate(testDate);
		itemPluDto.setDescription("description456");
		itemPluDto.setDiscCode("discCode123");
		itemPluDto.setSku(new Decimal(67890));
	}
	
	private Date makeADate(String dateString) throws ParseException {	
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		return dateFormat.parse(dateString);
	}
	
}
