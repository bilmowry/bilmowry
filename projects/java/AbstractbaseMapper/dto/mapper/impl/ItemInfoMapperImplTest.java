package com.menards.pos.ws.common.domain.dto.mapper.impl;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.menards.pos.ws.common.domain.dto.entity.ItemInfoDto;
import com.menards.pos.ws.common.domain.dto.mapper.ItemInfoMapper;
import com.menards.pos.ws.common.domain.entity.menard.ItemInfoEntity;
import com.menards.pos.ws.common.domain.value.Decimal;

public class ItemInfoMapperImplTest {
	
	ItemInfoMapper itemInfoMapper;
	
	private ItemInfoEntity itemInfoEntity;		
	private ItemInfoDto itemInfoDto;

	@Before
	public void setUp() throws Exception {
		itemInfoMapper = new ItemInfoMapperImpl();
		itemInfoEntity = new ItemInfoEntity();		
		itemInfoDto = new ItemInfoDto();
	}

	@After
	public void tearDown() throws Exception {
		itemInfoMapper = null;
		itemInfoEntity = null;
		itemInfoDto = null;
	}

	@Test
	public void testToDto() {
		populateEntity() ;
		itemInfoDto = itemInfoMapper.toDTO(itemInfoEntity, ItemInfoDto.class);
		
		assertEquals(itemInfoDto.getNotes(),"notes entity");
		assertEquals(itemInfoDto.getOverstockQuantity(), new Decimal(13));
		assertEquals(itemInfoDto.getPrepaidQuantity(), new Decimal (17));
		assertEquals(itemInfoDto.getSku(), new Decimal(12345));

	}
	
	@Test
	public void testFromDto() {
		populateDto() ;
		itemInfoEntity = itemInfoMapper.fromDTO(itemInfoDto, ItemInfoEntity.class);
		
		assertEquals(itemInfoEntity.getNotes(),"notes dto");
		assertEquals(itemInfoEntity.getOverstockQuantity(), new Decimal(23));
		assertEquals(itemInfoEntity.getPrepaidQuantity(), new Decimal (27));
		assertEquals(itemInfoEntity.getSku(), new Decimal(67890));

	}
	
	private void populateEntity() {
		itemInfoEntity.setNotes("notes entity");
		itemInfoEntity.setOverstockQuantity(new Decimal(13));
		itemInfoEntity.setPrepaidQuantity(new Decimal (17));
		itemInfoEntity.setSku(new Decimal(12345));
	}
	
	private void populateDto() {
		itemInfoDto.setNotes("notes dto");
		itemInfoDto.setOverstockQuantity(new Decimal(23));
		itemInfoDto.setPrepaidQuantity(new Decimal (27));
		itemInfoDto.setSku(new Decimal(67890));
	}

}
