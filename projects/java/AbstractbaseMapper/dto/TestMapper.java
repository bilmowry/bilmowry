package com.menards.pos.ws.common.domain.dto;

public interface TestMapper {
	public <T,O> T fromDTO(O fromDTO, Class<T> toEntity);
	public <T,O> T toDTO(O fromEntity, Class<T> toDTO);
}
