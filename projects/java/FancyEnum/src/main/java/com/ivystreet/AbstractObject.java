package com.ivystreet;

public abstract class AbstractObject {
    public abstract void run(String name);
}
