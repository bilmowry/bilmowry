package com.ivystreet;

public enum MethodEnum {
    NAVY {
        @Override
        public void shoutCheer(String name) {
            System.out.println("method navy " + name);
        }

    },
    ORANGE {
        @Override
        public void shoutCheer(String name) {
            System.out.println("method orange " + name);
        }
    },
    WHITE {
        @Override
        public void shoutCheer(String name) {
            System.out.println(" method white " + name);
        }
    };

    public abstract void shoutCheer(String name);

}
