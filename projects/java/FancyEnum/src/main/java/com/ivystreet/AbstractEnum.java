package com.ivystreet;

public enum AbstractEnum {
    NAVY(new AbstractObject() {
        @Override
        public void run(String name) {
            System.out.println("abstract navy " + name);
        }
    }), ORANGE(new AbstractObject() {
        @Override
        public void run(String name) {
            System.out.println("abstract orange " + name);
        }
    }), WHITE(new AbstractObject() {
        @Override
        public void run(String name) {
            System.out.println("abstract white " + name);
        }
    });

    private AbstractObject runner;

    private AbstractEnum(AbstractObject runner) {
        this.runner = runner;
    };

    public void process(String name) {
        this.runner.run(name);
    }

}
