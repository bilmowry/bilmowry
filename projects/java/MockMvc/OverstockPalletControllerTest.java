package com.menards.pos.ws.common.web.controller;

import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.menards.pos.ws.common.domain.entity.json.overstock.PalletDetail;
import com.menards.pos.ws.common.domain.entity.json.overstock.PalletHeader;
import com.menards.pos.ws.common.domain.entity.json.overstock.request.OverstockPalletDeleteRequest;
import com.menards.pos.ws.common.domain.entity.json.overstock.request.OverstockPalletRestoreRequest;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletAddResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletDeleteResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletDeletedResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletListResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletRestoreResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletUpdateResponse;
import com.menards.pos.ws.common.domain.value.Decimal;
import com.menards.pos.ws.common.service.overstock.OverstockPalletService;

@RunWith(MockitoJUnitRunner.class)
public class OverstockPalletControllerTest {
    private MockMvc mockMvc;

    @InjectMocks
    private OverstockPalletController overstockPalletController = new OverstockPalletController();

    @Mock
    private OverstockPalletService mockOverstockPalletService;

    /* @formatter:off */

    @Before
    public void setup() {
	MockitoAnnotations.initMocks(this);
	mockMvc = MockMvcBuilders.standaloneSetup(overstockPalletController).alwaysExpect(status().isOk())
		.alwaysExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE)).build();
    }

    @Test
    public void testList() throws Exception {
	final List<Integer> ids = new ArrayList<Integer>();
	ids.add(Integer.valueOf(1));
	ids.add(Integer.valueOf(2));
	final OverstockPalletListResponse response = new OverstockPalletListResponse(ids);

	Mockito.when(mockOverstockPalletService.list()).thenReturn(response);

	mockMvc.perform(get("/pallet/list").accept(MediaType.APPLICATION_JSON)).andExpect(
		jsonPath("$.OverstockPalletListResponse.pallets").isArray());
    }

    @Test
    public void testDeleted() throws Exception {
	final List<Integer> ids = new ArrayList<Integer>();
	ids.add(Integer.valueOf(1));
	ids.add(Integer.valueOf(2));
	final OverstockPalletDeletedResponse response = new OverstockPalletDeletedResponse(ids);

	Mockito.when(mockOverstockPalletService.deleted()).thenReturn(response);

	mockMvc.perform(get("/pallet/list/deleted").accept(MediaType.APPLICATION_JSON)).andExpect(
		jsonPath("$.OverstockPalletDeletedResponse.pallets").isArray());
    }

    @Test
    public void testSinglePallet() throws Exception {
	final List<PalletDetail> details = new ArrayList<PalletDetail>();
	final PalletDetail detail = new PalletDetail(1021101, "JUNIT 2x4", Decimal.valueOf(100), Decimal.valueOf(200));
	details.add(detail);
	final PalletHeader header = new PalletHeader(1, "JUNIT", new Date(), "summer", null, false, 200,
		"JUNIT TESTER", "TEST PALLET", new Date(), details);
	final PalletHeader response = header;

	Mockito.when(mockOverstockPalletService.find(1)).thenReturn(response);

	mockMvc.perform(get("/pallet/find/1").accept(MediaType.APPLICATION_JSON)).andExpect(
		jsonPath("$.PalletHeader.palletId").value(1));
    }

    @Test
    public void testDeletePallet() throws Exception {
	final OverstockPalletDeleteRequest delRequest = new OverstockPalletDeleteRequest();
	delRequest.setPalletId(1);
	delRequest.setTeamMemberName("JUNIT TESTER");

	final OverstockPalletDeleteResponse response = new OverstockPalletDeleteResponse(1, true);

	Mockito.when(mockOverstockPalletService.delete(any(OverstockPalletDeleteRequest.class))).thenReturn(response);

	final String request = new ObjectMapper().writeValueAsString(delRequest);

	mockMvc.perform(
		post("/pallet/delete").contentType(MediaType.APPLICATION_JSON).content(request)
			.accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.OverstockPalletDeleteResponse.PalletID").value(1))
		.andExpect(jsonPath("$.OverstockPalletDeleteResponse.ValidDelete").value(true));
    }

    @Test
    public void testRestorePallet() throws Exception {
	final OverstockPalletRestoreRequest restoreRequest = new OverstockPalletRestoreRequest();
	restoreRequest.setPalletId(1);
	restoreRequest.setTeamMemberName("JUNIT TESTER");

	final OverstockPalletRestoreResponse response = new OverstockPalletRestoreResponse(1, true);

	Mockito.when(mockOverstockPalletService.restore(any(OverstockPalletRestoreRequest.class))).thenReturn(response);

	final String request = new ObjectMapper().writeValueAsString(restoreRequest);

	mockMvc.perform(
		post("/pallet/restore").contentType(MediaType.APPLICATION_JSON).content(request)
			.accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.OverstockPalletRestoreResponse.PalletID").value(1))
		.andExpect(jsonPath("$.OverstockPalletRestoreResponse.ValidRestore").value(true));
    }

    @Test
    public void testAddPallet() throws Exception {
	final List<PalletDetail> details = new ArrayList<PalletDetail>();
	final PalletDetail detail = new PalletDetail(1021101, "JUNIT 2x4", Decimal.valueOf(100), Decimal.valueOf(200));
	details.add(detail);
	final PalletHeader header = new PalletHeader(1, "JUNIT", new Date(), "summer", null, false, 200,
		"JUNIT TESTER", "TEST PALLET", new Date(), details);

	final String request = new ObjectMapper().writeValueAsString(header);

	final OverstockPalletAddResponse response = new OverstockPalletAddResponse();
	response.setPalletId(1);

	Mockito.when(mockOverstockPalletService.add(any(PalletHeader.class))).thenReturn(response);

	mockMvc.perform(
		put("/pallet/add").contentType(MediaType.APPLICATION_JSON).content(request)
			.accept(MediaType.APPLICATION_JSON)).andExpect(
		jsonPath("$.OverstockPalletAddResponse.PalletID").value(1));
    }

    @Ignore
    @Test
    public void testUpdatePallet() throws Exception {
	final List<PalletDetail> details = new ArrayList<PalletDetail>();
	final PalletDetail detail = new PalletDetail(1021101, "JUNIT 2x4", Decimal.valueOf(100), Decimal.valueOf(200));
	details.add(detail);
	final PalletHeader header = new PalletHeader(1, "JUNIT", new Date(), "summer", null, false, 200,
		"JUNIT TESTER", "TEST PALLET", new Date(), details);

	final ObjectMapper mapper = new ObjectMapper();
	final String request = mapper.writeValueAsString(header);

	final OverstockPalletUpdateResponse response = new OverstockPalletUpdateResponse();
	response.setPalletId(1);
	response.setValidUpdate(true);

	Mockito.when(mockOverstockPalletService.update(any(PalletHeader.class))).thenReturn(response);

	mockMvc.perform(
		post("/pallet/update").contentType(MediaType.APPLICATION_JSON).content(request)
			.accept(MediaType.APPLICATION_JSON)).andExpect(
		jsonPath("$.OverstockPalletUpdateResponse.PalletID").value(1));
    }

    /* @formatter:on */
}