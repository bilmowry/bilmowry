package com.menards.pos.ws.common.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.menards.pos.ws.common.domain.entity.json.overstock.PalletHeader;
import com.menards.pos.ws.common.domain.entity.json.overstock.request.OverstockPalletDeleteRequest;
import com.menards.pos.ws.common.domain.entity.json.overstock.request.OverstockPalletRestoreRequest;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletAddResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletDeleteResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletDeletedResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletListResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletRestoreResponse;
import com.menards.pos.ws.common.domain.entity.json.overstock.response.OverstockPalletUpdateResponse;
import com.menards.pos.ws.common.service.overstock.OverstockPalletService;

/**
 * Primary entry point to the Overstock Controls<br/>
 * 
 * TODO - finish mappings for other operations
 * 
 * @author alandsve
 */
@Controller
@RequestMapping(value = "/pallet/**", produces = MediaType.APPLICATION_JSON_VALUE)
public class OverstockPalletController
{
	private final Logger log = LoggerFactory.getLogger(OverstockPalletController.class);

	@Autowired
	private OverstockPalletService overstockPalletService;

	/**
	 * Returns a list of all the pallets that aren't deleted
	 * 
	 * @return OverstockPalletListResponse
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public @ResponseBody OverstockPalletListResponse list()
	{
		log.debug("Getting list of pallets");

		return overstockPalletService.list();
	}
	
	/**
	 * Returns a list of all the pallets that are deleted
	 * 
	 * @return OverstockPalletDeletedResponse
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/list/deleted")
	public @ResponseBody OverstockPalletDeletedResponse deleted()
	{
		log.debug("Getting list of pallets");

		return overstockPalletService.deleted();
	}

	/**
	 * Returns a PalletHeader object that references one pallet
	 * 
	 * @param palletId Pallet ID to search for
	 * 
	 * @return OverstockPalletViewResponse
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/find/{palletId}")
	public @ResponseBody PalletHeader find(@PathVariable Integer palletId)
	{
		log.debug("Searching for pallet by ID: {}", palletId);

		return overstockPalletService.find(palletId);
	}

	/**
	 * Marks a pallet as deleted and returns a response based on if it was successful or not
	 * 
	 * @param request OverstockPalletDeleteRequest
	 * @return OverstockPalletDeleteResponse
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/delete")
	public @ResponseBody OverstockPalletDeleteResponse delete(@RequestBody OverstockPalletDeleteRequest request)
	{
		log.debug("Deleting pallet by ID: {}", request.getPalletId());

		return overstockPalletService.delete(request);
	}
	
	/**
	 * Restores a pallet's status as active
	 * 
	 * @param request OverstockPalletRestoreRequest
	 * @return OverstockPalletRestoreResponse
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/restore")
	public @ResponseBody OverstockPalletRestoreResponse restore(@RequestBody OverstockPalletRestoreRequest request)
	{
		log.debug("Restoring pallet by ID: {}", request.getPalletId());

		return overstockPalletService.restore(request);
	}

	/**
	 * Adds a new pallet to the DB and updates appropriate tables
	 * 
	 * @param pallet PalletHeader
	 * @return OverstockPalletAddResponse
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/add")
	public @ResponseBody OverstockPalletAddResponse add(@RequestBody PalletHeader pallet)
	{
		log.debug("Adding new pallet");

		return overstockPalletService.add(pallet);
	}

	/**
	 * Updates a pallet in the DB
	 * 
	 * @param pallet PalletHeader
	 * @return OverstockPalletUpdateResponse
	 */
	//@RequestMapping(method = RequestMethod.POST, value = "/update")
	public @ResponseBody OverstockPalletUpdateResponse update(@RequestBody PalletHeader pallet)
	{
		log.debug("Updating pallet {}", pallet.getPalletId());

		return overstockPalletService.update(pallet);
	}
}