/*
Computes all Armstrong numbers in the range of 0 and 999.  
An Armstrong number is a number such that the sum
 of its digits raised to the third power is equal to the number
 itself.  For example, 371 is an Armstrong number, since
 3**3 + 7**3 + 1**3 = 371.
*/

def i = 0

while ( i++ <= 999 ) {
    def numString = String.format("%d",i)
    def replacement = {
          String k = "" + it + ""
    }
    def x = 0;
    numString.collectReplacements(replacement).each() {
        Integer j = it.toInteger()
        x+= j**3
    }
    
    if (x == i) { println x + " = " + i + " Armstrong" } 
}