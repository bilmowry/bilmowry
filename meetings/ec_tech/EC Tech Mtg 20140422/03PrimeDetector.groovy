/*
Prime number detector -- detect prim numbers from 1 to 100
*/

def i = 0

while ( i++ < 100 ) {

   def j = 0
   def ct = 0
   while( j++ <= 100) {
       if (i % j == 0) {
           ct++
       }
   }
   
   if (ct <= 2) {println i + " is a prime"}

}