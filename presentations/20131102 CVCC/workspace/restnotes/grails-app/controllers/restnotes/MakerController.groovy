package restnotes

import grails.converters.deep.JSON
import grails.converters.XML

class MakerController {

    def list() {
        def all = Manufacturer.list()
        render all as JSON
    }

    def listx() {
        def all = Manufacturer.list()
        render all as XML
    }

    def show() {
        def m = Manufacturer.get(params.id)
        render m as JSON
    }

    def save() {
        def jsonObject = request.JSON
        def b = new Manufacturer(jsonObject)
        if (b.save()) {
            render b as JSON
        }
        else {
            render b.errors
        }
    }

    def update() {
        def jsonObject = request.JSON
        def b = new Manufacturer(jsonObject)
        if (b.save()) {
            render b as JSON
        }
        else {
            render b.errors
        }
    }

    def delete() {
        Manufacturer.get(params.id).delete()
    }

    def index() { }
}
