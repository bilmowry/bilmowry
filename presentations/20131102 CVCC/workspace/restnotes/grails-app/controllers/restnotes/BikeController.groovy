package restnotes
import grails.converters.JSON
import grails.converters.XML

class BikeController {

    def list() {
            def all = Bicycle.list()
            render all as JSON
    }

    def listx() {
        def all = Bicycle.list()
        render all as XML
    }

    def show() {
        def m = Bicycle.get(params.id)
        render m as JSON
    }

    def save() {
        def jsonObject = request.JSON
        def b = new Bicycle(jsonObject)
        if (b.save()) {
            render b as JSON
        }
        else {
            render b.errors
        }
    }

    def update() {
        def jsonObject = request.JSON
        def b = new Bicycle(jsonObject)
        if (b.save()) {
            render b as JSON
        }
        else {
            render b.errors
        }
    }

    def delete() {
        Bicycle.get(params.id).delete()

        render  "delete complete"
    }

}
