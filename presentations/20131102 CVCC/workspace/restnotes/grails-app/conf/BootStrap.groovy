import restnotes.Manufacturer
import restnotes.Bicycle

class BootStrap {

    def init = { servletContext ->
       new Manufacturer(name:  "Motobecane", mailOrRetail: "mail", location:  "France")
               .addToBicycles(new Bicycle(style: "fixed", frame: "steel", groupo:  "Campagnolo", color: "orange"))
               .addToBicycles(new Bicycle(style: "road", frame: "carbon", groupo:  "Shimono", color: "black"))
               .save()
          System.out.print("Bootstrapping . . . ")

        new Manufacturer(name:  "Salsa", mailOrRetail: "mail", location:  "St. Paul")
                .addToBicycles(new Bicycle(style: "mountain", frame: "aluminum", groupo:  "Sram", color: "silver"))
                .save()
        System.out.print("Bootstrapping . . . ")
    }
    def destroy = {
    }
}