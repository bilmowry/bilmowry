class UrlMappings {
	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/maker/$id?"(resource:"maker")
		"/maker/list"(controller: "maker", action: "list")
		"/maker/listx"(controller: "maker", action: "listx")

		"/bike/$id?"(resource:"bike")
		"/bike/list"(controller: "bike", action: "list")
		"/bike/listx"(controller: "bike", action: "listx")

		"/"(view:"/index")
		"500"(view:'/error')
	}
}
