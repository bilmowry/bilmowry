package restnotes

//@Resource(uri='/bicycle')  <-- grails 2.3  does this for domains, is this a good idea?
class Bicycle {
    String style
    String frame
    String groupo
    String color

   static belongsTo = [manufacturer: Manufacturer]

}
