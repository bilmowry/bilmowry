11/2/2013 Chippewa Valley Code Camp

Contents of presentation for Development Lessons of ReST APIs

presented by Bil Mowry
----------------------

Development Lessons.ppt - power point presentation (created by Kingsoft office, works with MS)

workspace:

apidocs-example-master - grails example from the Grails apidocs plugin repository

java-grails2 - grails example from the Swagger api documentation repository

restnotes - grails application created by Bil Mowry, Ivy Street Technology LLC for presentation.

Thanks!